package com.citylib.webapp.controller;

import com.citylib.webapp.config.ServiceRoutes;
import com.citylib.webapp.dto.book.Book;
import com.citylib.webapp.dto.library.BookReservationPerLibrary;
import com.citylib.webapp.dto.library.LibraryWithCopies;
import com.citylib.webapp.dto.misc.Paging;
import com.citylib.webapp.model.User;
import com.citylib.webapp.service.UserService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.security.Principal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/books")
public class BooksController {

    @Autowired
    UserService userService;
    @Autowired
    RestTemplate restTemplate;
    @Autowired
    ServiceRoutes serviceRoutes;

    DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    Logger logger = LoggerFactory.getLogger(BooksController.class);

    private ObjectMapper objectMapper = new ObjectMapper();

    @GetMapping(value = "", produces = "application/json")
    public String getBooks(
            @RequestParam(value="pageNum", required=false) Integer pageNum,
            @RequestParam(value="pageSize", required=false) Integer pageSize,
            @RequestParam(value="pageSort", required=false) String pageSort,
            @RequestParam(value="pageSearch", required=false) String pageSearch,
            @ModelAttribute("currentPage") Paging page, Model model) throws IOException {

        // Checks for a change in page number
        if(pageNum != null) {
            page.setPage(pageNum);
        }
        // Checks for a change in page size
        if(pageSize != null) {
            page.setSize(pageSize);
            page.setPage(1);
        }
        // Checks for a change in sort type
        if(pageSort != null) {
            page.setSort(pageSort);
            page.setPage(1);
        }
        // Checks for a change in search query
        if(pageSearch != null) {
            page.setSearch(pageSearch);
            page.setPage(1);
        }
        // Initialises the query object if it hasn't been used before
        if(page.getPage() == null && page.getSize() == null && page.getSort() == null && page.getSearch() == null) {
            page = new Paging(1, 10 , "title", "");
        }

        // Builds the uri to query the book service
        StringBuilder uri = new StringBuilder();
        uri.append(serviceRoutes.getBooks())
                .append("?")
                .append("page=").append(page.getPage())
                .append("&")
                .append("size=").append(page.getSize())
                .append("&")
                .append("sort=").append(page.getSort())
                .append("&")
                .append("search=").append(page.getSearch());

        ResponseEntity<String> response = restTemplate.getForEntity(uri.toString(), String.class);

        // If good response from microservice, it builds the objects to display in application
        if(response.getStatusCode() == HttpStatus.OK && response.getBody() != null){
            ObjectNode responseObject = objectMapper.readValue(response.getBody(), ObjectNode.class);
            List<Book> bookList = objectMapper.readValue(responseObject.get("content").toString(), new TypeReference<List<Book>>(){});
            for(Book book : bookList) {
                book.setReleaseDate(convertDateFormat(book.getReleaseDate()));
            }
            model.addAttribute("bookList", bookList);
            model.addAttribute("currentPage", page);
            model.addAttribute("totalPages", Integer.parseInt(responseObject.get("totalPages").toString()));
        } else {
            model.addAttribute("errorRetrieving", true);
        }
        return "/books/allBooks";
    }

    @RequestMapping(value = "/{id}", produces = "application/json")
    public String getBook(@PathVariable("id") long id, Model model, Principal principal) throws IOException, JSONException {
        try {
            //Retrieves book information from book microservice
            ResponseEntity<String> responseBook = restTemplate.getForEntity(serviceRoutes.getBookById(id), String.class);

            // Retrieves list of reservations for book
            ResponseEntity<String> responseBookReservations = restTemplate.getForEntity(serviceRoutes.getReservationsById(id), String.class);
            ArrayList<BookReservationPerLibrary> reservationList = objectMapper.readValue(responseBookReservations.getBody(), new TypeReference<List<BookReservationPerLibrary>>(){});

            //Returns list of copies associated with the book from library microservice
            ResponseEntity<String> responseCopyList = restTemplate.getForEntity(serviceRoutes.getCopyListById(id), String.class);
            Book book = objectMapper.readValue(responseBook.getBody(), Book.class);
            book.setReleaseDate(convertDateFormat(book.getReleaseDate()));
            ArrayList<LibraryWithCopies> libraryWithCopies = objectMapper.readValue(responseCopyList.getBody(), new TypeReference<List<LibraryWithCopies>>(){});

            // Adds reservation values to each library
            for (LibraryWithCopies eachLib : libraryWithCopies) {
                for (BookReservationPerLibrary eachReservation : reservationList) {
                    if(eachReservation.getLibraryId() == eachLib.getId()) {
                        eachLib.setReservationCount(eachReservation.getReservationCount());
                    }
                }
            }

            if (principal != null) {
                User user = userService.search(principal.getName());
                ResponseEntity<String> responseHasReservation = restTemplate.getForEntity(serviceRoutes.getUserHasReservation(book.getId(), user.getId()), String.class);
                model.addAttribute("userHasReservation", responseHasReservation.getBody());
            }

            model.addAttribute("book", book);
            model.addAttribute("copyList", libraryWithCopies);
            model.addAttribute("responseBody", responseBook.getBody());
        } catch (HttpClientErrorException e) {
            logger.info(e.getRawStatusCode() + ": " + e.getResponseBodyAsString());
            model.addAttribute("statusCode", e.getRawStatusCode());
            return "error";
        }
        return "books/singleBook";
    }

    public String convertDateFormat(String date) {
        return LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd")).format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
    }
}