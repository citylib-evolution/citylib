package com.citylib.webapp.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.WebUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

// Token will be checked against database data once per request
public class JwtTokenFilter extends OncePerRequestFilter {

  private JwtTokenProvider jwtTokenProvider;

  public JwtTokenFilter(JwtTokenProvider jwtTokenProvider) {
    this.jwtTokenProvider = jwtTokenProvider;
  }

  @Override
  protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
    String token = jwtTokenProvider.resolveToken(httpServletRequest);
    try {
      if (token != null && jwtTokenProvider.validateToken(token)) {
        Authentication auth = jwtTokenProvider.getAuthentication(token);
        SecurityContextHolder.getContext().setAuthentication(auth);
      }
    } catch (HttpClientErrorException | UsernameNotFoundException e) {
      SecurityContextHolder.clearContext();
      Cookie cookie = WebUtils.getCookie(httpServletRequest, "Authorization");
      cookie.setMaxAge(0);
      httpServletResponse.addCookie(cookie);
      httpServletResponse.setStatus(HttpServletResponse.SC_FORBIDDEN);
      httpServletResponse.setHeader("Location", "http://localhost:8080");
      return;
    }

    filterChain.doFilter(httpServletRequest, httpServletResponse);
  }

}
