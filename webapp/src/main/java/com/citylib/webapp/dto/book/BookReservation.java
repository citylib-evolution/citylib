package com.citylib.webapp.dto.book;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class BookReservation {
    private long id;

    private long userId;

    private long bookId;

    private long libraryId;

    private String reservedTime;

    private String notifiedTime;

    private int placement;

    private boolean notified;
}
