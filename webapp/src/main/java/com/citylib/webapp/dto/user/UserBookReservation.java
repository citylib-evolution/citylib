package com.citylib.webapp.dto.user;

import com.citylib.webapp.dto.book.Book;
import com.citylib.webapp.dto.library.Library;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class UserBookReservation {
    private long id;

    private Book book;

    private Library library;

    private String reservedTime;

    private String notifiedTime;

    private int placement;

    private boolean notified;
}
