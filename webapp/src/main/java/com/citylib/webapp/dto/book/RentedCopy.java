package com.citylib.webapp.dto.book;

import lombok.Data;

import java.time.LocalDate;

@Data
public class RentedCopy {
    private long id;
    private long userId;
    private String startDate;
    private boolean extended;
    private boolean returned;
    private BookCopy bookCopy;
}
