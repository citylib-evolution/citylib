package com.citylib.webapp.dto.library;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
public class BookReservationPerLibrary {
    long bookId;
    long libraryId;
    int reservationCount;
}
