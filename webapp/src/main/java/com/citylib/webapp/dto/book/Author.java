package com.citylib.webapp.dto.book;

import lombok.Data;

import java.util.Date;

@Data
public class Author {
    private long id;

    private String name;

    private String surname;

    private Date birthDate;
}
