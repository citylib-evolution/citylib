package com.citylib.library.service;

import com.citylib.library.dto.LibraryWithCopies;
import com.citylib.library.model.Address;
import com.citylib.library.model.BookCopy;
import com.citylib.library.model.Library;
import com.citylib.library.model.LibrarySchedule;
import com.citylib.library.repository.BookCopyRepository;
import com.citylib.library.repository.LibraryRepository;
import com.citylib.library.repository.RentedCopyRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

class BookCopyServiceTest {
    @Mock
    private BookCopyRepository bookCopyRepository;
    @Mock
    private LibraryRepository libraryRepository;
    @Mock
    private RentedCopyRepository rentedCopyRepository;

    @Spy
    @InjectMocks
    BookCopyService bookCopyService;

    private Library library;
    private BookCopy bookCopy;

    @BeforeEach
    void setUp() {
        initMocks(this);

        library = new Library();
        library.setId(1);
        library.setName("Lib1");
        library.setAddress(mock(Address.class));
        library.setLibrarySchedule(mock(LibrarySchedule.class));

        bookCopy = new BookCopy();
        bookCopy.setId(1);
        bookCopy.setBookId(1);
        bookCopy.setLibrary(library);
    }

    @Test
    void findAllCopiesByBookId() {
        List<Library> libraryList = new ArrayList<>();
        List<BookCopy> bookCopyList = new ArrayList<>();
        libraryList.add(library);
        bookCopyList.add(bookCopy);

        when(libraryRepository.findAll()).thenReturn(libraryList);
        when(bookCopyRepository.countByLibraryIdAndBookId(1,1)).thenReturn(1);
        when(bookCopyRepository.findAllByLibraryAndBookId(library,1)).thenReturn(bookCopyList);

        List<LibraryWithCopies> result = bookCopyService.findAllCopiesByBookId(1);

        assertThat(result.get(0)).hasFieldOrPropertyWithValue("availableCount", 1);

        verify(libraryRepository).findAll();
        verify(bookCopyRepository).findAllByLibraryAndBookId(library,1);
    }

    @Test
    void findAvailableCopy() {
        List<BookCopy> bookCopyList = new ArrayList<>();
        bookCopyList.add(bookCopy);

        when(libraryRepository.findById(1)).thenReturn(library);
        when(bookCopyRepository.findAllByLibraryAndBookId(library, 1)).thenReturn(bookCopyList);
        doReturn(true).when(bookCopyService).copyAvailable(1);

        List<BookCopy> result = bookCopyService.findAvailableCopy(1,1);

        assertThat(result).hasSize(1);

        verify(libraryRepository).findById(1);
        verify(bookCopyRepository).findAllByLibraryAndBookId(library,1);
        verify(bookCopyService).copyAvailable(1);
    }

    @Test
    void copyAvailable() {
        when(bookCopyRepository.findById(1)).thenReturn(bookCopy, bookCopy);
        when(rentedCopyRepository.countByBookCopyAndReturned(bookCopy, false)).thenReturn(0,1);

        boolean result1 = bookCopyService.copyAvailable(1);
        boolean result2 = bookCopyService.copyAvailable(1);

        assertThat(result1).isTrue();
        assertThat(result2).isFalse();

        verify(bookCopyRepository, times(2)).findById(1);
        verify(rentedCopyRepository, times(2)).countByBookCopyAndReturned(bookCopy, false);
    }
}