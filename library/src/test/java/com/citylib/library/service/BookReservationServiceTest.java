package com.citylib.library.service;

import com.citylib.library.model.BookReservation;
import com.citylib.library.repository.BookReservationRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

class BookReservationServiceTest {
    @Mock
    private BookReservationRepository bookReservationRepository;

    @Spy
    @InjectMocks
    private BookReservationService bookReservationService;

    private BookReservation bookReservation;

    @BeforeEach
    void setUp() {
        initMocks(this);

        bookReservation = new BookReservation();
        bookReservation.setId(1);
        bookReservation.setUserId(1);
        bookReservation.setBookId(1);
        bookReservation.setLibraryId(1);
        bookReservation.setReservedTime(LocalDateTime.now());
        bookReservation.setNotified(false);
    }

    @Test
    void getBookReservationByLibrary() {
        List<BookReservation> bookReservationList = new ArrayList<>();
        bookReservationList.add(bookReservation);

        when(bookReservationRepository.findAllByBookIdAndLibraryIdOrderByReservedTimeAsc(1,1)).thenReturn(bookReservationList);

        List<BookReservation> result = bookReservationService.getBookReservationByLibrary(1,1);

        assertThat(result).isEqualTo(bookReservationList);

        verify(bookReservationRepository).findAllByBookIdAndLibraryIdOrderByReservedTimeAsc(1,1);
    }

    @Test
    void countBookReservationByLibrary() {
        when(bookReservationRepository.countAllByLibraryIdAndBookId(1,1)).thenReturn(1);

        int result = bookReservationService.countBookReservationByLibrary(1,1);

        assertThat(result).isEqualTo(1);

        verify(bookReservationRepository).countAllByLibraryIdAndBookId(1,1);

    }

    @Test
    void getReservationPlacement() {
        List<BookReservation> bookReservationList = new ArrayList<>();
        bookReservationList.add(bookReservation);

        doReturn(bookReservationList).when(bookReservationService).getBookReservationByLibrary(1,1);

        int result = bookReservationService.getReservationPlacement(1,1,1);

        assertThat(result).isEqualTo(0);

        verify(bookReservationService).getBookReservationByLibrary(1,1);

//        long bookId, long libraryId, long userId) {
//        List<BookReservation> reservationList = getBookReservationByLibrary(bookId, libraryId);
//        int i = 0;
//        for(BookReservation reservation : reservationList) {
//            if(reservation.getUserId() == userId) {
//                break;
//            }
//            i++;
//        }
//        return i;
//    }
    }
}