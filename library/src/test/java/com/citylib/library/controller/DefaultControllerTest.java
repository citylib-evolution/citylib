package com.citylib.library.controller;

import com.citylib.library.dto.LibraryWithCopies;
import com.citylib.library.model.*;
import com.citylib.library.repository.LibraryRepository;
import com.citylib.library.repository.RentedCopyRepository;
import com.citylib.library.service.BookCopyService;
import com.citylib.library.service.RentedCopyService;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import java.awt.print.Book;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class DefaultControllerTest {
    @Mock
    private RentedCopyService rentedCopyService;
    @Mock
    private BookCopyService bookCopyService;
    @Mock
    private LibraryRepository libraryRepository;
    @Mock
    private RentedCopyRepository rentedCopyRepository;

    @Mock
    private Address address;
    @Mock
    private LibrarySchedule librarySchedule;
    @Mock
    private LibraryWithCopies libraryWithCopies;

    private BookCopy bookCopy;
    private Library library;
    private RentedCopy rentedCopy;

    @Spy
    @InjectMocks
    private DefaultController defaultController;

    @BeforeEach
    void setUp() {
        initMocks(this);
        defaultController.setRentTime(5);

        bookCopy = new BookCopy();
        bookCopy.setId(1);
        bookCopy.setBookId(1);
        bookCopy.setLibrary(library);

        library = new Library();
        library.setId(1);
        library.setName("Lib1");
        library.setAddress(address);
        library.setLibrarySchedule(librarySchedule);

        rentedCopy = new RentedCopy();
        rentedCopy.setId(1);
        rentedCopy.setUserId(1);
        rentedCopy.setStartDate(LocalDate.now());
        rentedCopy.setExtended(true);
        rentedCopy.setReturned(false);
        rentedCopy.setBookCopy(bookCopy);

    }

    @Test
    void getLibrary() {
        when(libraryRepository.findById(1)).thenReturn(library);

        ResponseEntity result = defaultController.getLibrary(1);

        assertThat(result).hasFieldOrPropertyWithValue("body", library).hasFieldOrPropertyWithValue("statusCode", HttpStatus.OK);

        verify(libraryRepository).findById(1);
    }

    @Test
    void getBookCopyList() {
        List<LibraryWithCopies> libraryWithCopiesList = new ArrayList<>();
        libraryWithCopiesList.add(libraryWithCopies);
        libraryWithCopiesList.add(libraryWithCopies);

        when(bookCopyService.findAllCopiesByBookId(1)).thenReturn(libraryWithCopiesList);

        ResponseEntity result = defaultController.getBookCopyList(1);

        assertThat(result).hasFieldOrPropertyWithValue("body", libraryWithCopiesList).hasFieldOrPropertyWithValue("statusCode", HttpStatus.OK);

        verify(bookCopyService).findAllCopiesByBookId(1);
    }

    @Test
    void getRentedCopiesList() {
        List<RentedCopy> rentedCopyList = new ArrayList<>();
        rentedCopyList.add(rentedCopy);
        rentedCopyList.add(rentedCopy);

        when(rentedCopyRepository.findAllByUserId(1)).thenReturn(rentedCopyList);

        ResponseEntity result = defaultController.getRentedCopiesList(1);

        assertThat(result).hasFieldOrPropertyWithValue("body", rentedCopyList).hasFieldOrPropertyWithValue("statusCode", HttpStatus.OK);

        verify(rentedCopyRepository).findAllByUserId(1);
    }

    @Nested
    class putRentCopyTests {

        @Test
        void putRentCopyHasAvailable() {
            List<BookCopy> bookCopyList = new ArrayList<>();
            bookCopyList.add(bookCopy);
            bookCopyList.add(bookCopy);

            when(bookCopyService.findAvailableCopy(1,1)).thenReturn(bookCopyList);
            when(rentedCopyService.save(any())).thenReturn(rentedCopy);

            ResponseEntity result = defaultController.putRentCopy(1,1,1);

            assertThat(result).hasFieldOrPropertyWithValue("body", rentedCopy).hasFieldOrPropertyWithValue("statusCode", HttpStatus.OK);

            verify(bookCopyService).findAvailableCopy(1,1);
            verify(rentedCopyService).save(any());
        }

        @Test
        void putRentCopyNoneAvailable() {
            List<BookCopy> bookCopyList = new ArrayList<>();

            when(bookCopyService.findAvailableCopy(1,1)).thenReturn(bookCopyList);

            ResponseEntity result = defaultController.putRentCopy(1,1,1);

            assertThat(result).hasFieldOrPropertyWithValue("body", "{ error: 'No copies available.' }")
                    .hasFieldOrPropertyWithValue("statusCode", HttpStatus.OK);

            verify(bookCopyService).findAvailableCopy(1,1);
        }
    }


    @Nested
    class returnCopyTests {

        @AfterEach
        void afterEach() {
            rentedCopy = null;
        }

        @Test
        void returnCopyReturn() {

            ArgumentCaptor<RentedCopy> rentedCopyArgumentCaptor = ArgumentCaptor.forClass(RentedCopy.class);

            when(rentedCopyRepository.findById(1)).thenReturn(rentedCopy);
            when(rentedCopyRepository.save(rentedCopyArgumentCaptor.capture())).thenReturn(rentedCopy);

            assertThat(defaultController.returnCopy(1,1)).hasFieldOrPropertyWithValue("statusCode", HttpStatus.OK);

            verify(rentedCopyRepository).findById(1);
            verify(rentedCopyRepository).save(rentedCopyArgumentCaptor.getValue());
        }

        @Test
        void returnCopyAlreadyReturned() {
            rentedCopy.setReturned(true);

            when(rentedCopyRepository.findById(1)).thenReturn(rentedCopy);

            assertThat(defaultController.returnCopy(1,1)).hasFieldOrPropertyWithValue("statusCode", HttpStatus.UNPROCESSABLE_ENTITY);

            verify(rentedCopyRepository).findById(1);
        }
    }

    @Nested
    class extendRentedCopyTests {

        @Test
        void extendRentedCopyAlreadyExtended() {
            rentedCopy.setExtended(true);

            when(rentedCopyRepository.findById(1)).thenReturn(rentedCopy);

            ResponseEntity result = defaultController.extendRentedCopy(1);

            assertThat(result).hasFieldOrPropertyWithValue("body", "{error: 'Rented copy is already extended'}")
                    .hasFieldOrPropertyWithValue("statusCode", HttpStatus.UNPROCESSABLE_ENTITY);

            verify(rentedCopyRepository).findById(1);
        }

        @Test
        void extendRentedCopyPastDueDate() {
            rentedCopy.setExtended(false);
            rentedCopy.setStartDate(rentedCopy.getStartDate().minusMonths(3));

            when(rentedCopyRepository.findById(1)).thenReturn(rentedCopy);

            ResponseEntity result = defaultController.extendRentedCopy(1);

            assertThat(result).hasFieldOrPropertyWithValue("body", "{\nerror: 'Rented copy cannot be extend after it's due date (Due on " + rentedCopy.getStartDate().plusDays(defaultController.getRentTime()) + ").'\n}")
                    .hasFieldOrPropertyWithValue("statusCode", HttpStatus.UNPROCESSABLE_ENTITY);

            verify(rentedCopyRepository).findById(1);
        }

        @Test
        void extendRentedCopyOK() {
            rentedCopy.setExtended(false);

            when(rentedCopyRepository.findById(1)).thenReturn(rentedCopy);
            when(rentedCopyRepository.save(rentedCopy)).thenReturn(rentedCopy);

            ResponseEntity result = defaultController.extendRentedCopy(1);

            assertThat(result).hasFieldOrPropertyWithValue("body", "true")
                    .hasFieldOrPropertyWithValue("statusCode", HttpStatus.OK);

            verify(rentedCopyRepository).findById(1);
            verify(rentedCopyRepository).save(rentedCopy);
        }
    }

    @Test
    void getLibraryList() {
        List<Library> libraryList = new ArrayList<>();
        when(libraryRepository.findAll()).thenReturn(libraryList);

        ResponseEntity result = defaultController.getLibraryList();

        assertThat(result).hasFieldOrPropertyWithValue("body", libraryList).hasFieldOrPropertyWithValue("statusCode", HttpStatus.OK);

        verify(libraryRepository).findAll();
    }
}