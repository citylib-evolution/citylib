package com.citylib.library.controller;

import com.citylib.library.dto.BookReservationPerLibrary;
import com.citylib.library.model.Address;
import com.citylib.library.model.BookReservation;
import com.citylib.library.model.Library;
import com.citylib.library.model.LibrarySchedule;
import com.citylib.library.repository.BookCopyRepository;
import com.citylib.library.repository.BookReservationRepository;
import com.citylib.library.repository.LibraryRepository;
import com.citylib.library.service.BookReservationService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

class ReservationControllerTest {
    @Mock
    private LibraryRepository libraryRepository;
    @Mock
    private BookReservationService bookReservationService;
    @Mock
    private BookReservationRepository bookReservationRepository;
    @Mock
    private BookCopyRepository bookCopyRepository;
    @Mock
    private Address address;
    @Mock
    private LibrarySchedule librarySchedule;

    private BookReservation bookReservation;
    private Library library;

    @Spy
    @InjectMocks
    private ReservationController reservationController;

    @BeforeEach
    void setUp() {
        initMocks(this);

        bookReservation = new BookReservation();
        bookReservation.setId(1);
        bookReservation.setUserId(1);
        bookReservation.setBookId(1);
        bookReservation.setLibraryId(1);
        bookReservation.setReservedTime(LocalDateTime.now());
        bookReservation.setNotified(false);

        library = new Library();
        library.setId(1);
        library.setName("Lib1");
        library.setAddress(address);
        library.setLibrarySchedule(librarySchedule);
    }

    @Nested
    class userHasReservationTests {
        @Test
        void userHasReservationTrue() {
            when(bookReservationRepository.countAllByBookIdAndUserId(1,1)).thenReturn(1);

            ResponseEntity result = reservationController.userHasReservation(1,1);

            assertThat(result).hasFieldOrPropertyWithValue("body", "true").hasFieldOrPropertyWithValue("statusCode", HttpStatus.OK);

            verify(bookReservationRepository).countAllByBookIdAndUserId(1,1);
        }

        @Test
        void userHasReservationFalse() {
            when(bookReservationRepository.countAllByBookIdAndUserId(1,1)).thenReturn(0);

            ResponseEntity result = reservationController.userHasReservation(1,1);

            assertThat(result).hasFieldOrPropertyWithValue("body", "false").hasFieldOrPropertyWithValue("statusCode", HttpStatus.OK);

            verify(bookReservationRepository).countAllByBookIdAndUserId(1,1);
        }
    }

    @Test
    void getUserBookReservation() {
        List<BookReservation> bookReservationListEmpty = new ArrayList<>();
        List<BookReservation> bookReservationList = new ArrayList<>();
        bookReservationList.add(bookReservation);

        when(bookReservationRepository.findAllByUserId(1)).thenReturn(bookReservationList);
        when(bookReservationRepository.findAllByUserId(2)).thenReturn(bookReservationListEmpty);
        when(bookReservationService.getReservationPlacement(1,1,1)).thenReturn(1);

        ResponseEntity result = reservationController.getUserBookReservation(1);
        ResponseEntity resultEmpty = reservationController.getUserBookReservation(2);

        assertThat(result).hasFieldOrPropertyWithValue("body", bookReservationList).hasFieldOrPropertyWithValue("statusCode", HttpStatus.OK);
        assertThat(resultEmpty).hasFieldOrPropertyWithValue("body", bookReservationListEmpty).hasFieldOrPropertyWithValue("statusCode", HttpStatus.OK);

        verify(bookReservationRepository).findAllByUserId(1);
        verify(bookReservationRepository).findAllByUserId(2);
        verify(bookReservationService).getReservationPlacement(1,1,1);
    }

    @Test
    void getBookReservations() {
        List<Library> libraryList = new ArrayList<>();
        libraryList.add(library);

        List<BookReservationPerLibrary> bookReservationPerLibraryList = new ArrayList<>();
        BookReservationPerLibrary bookReservationPerLibrary = new BookReservationPerLibrary(1,1,1);
        bookReservationPerLibraryList.add(bookReservationPerLibrary);

        when(libraryRepository.findAll()).thenReturn(libraryList);
        when(bookReservationService.countBookReservationByLibrary(1, 1)).thenReturn(1);

        ResponseEntity result = reservationController.getBookReservations(1);

        System.out.println(result.getBody());

        List<BookReservationPerLibrary> resultCheck = (List<BookReservationPerLibrary>)result.getBody();
        assertThat(resultCheck.get(0)).hasFieldOrPropertyWithValue("bookId", 1L)
                .hasFieldOrPropertyWithValue("libraryId", 1L)
                .hasFieldOrPropertyWithValue("reservationCount", 1);
        assertThat(result).hasFieldOrPropertyWithValue("statusCode", HttpStatus.OK);

        verify(libraryRepository).findAll();
        verify(bookReservationService).countBookReservationByLibrary(1,1);
    }

    @Nested
    class postNewBookReservationTest {

        @Test
        void postNewBookReservationWithReservation() {
            when(bookReservationRepository.countAllByBookIdAndUserId(1,1)).thenReturn(1);

            ResponseEntity result = reservationController.postNewBookReservation(1,1,1);

            assertThat(result).hasFieldOrPropertyWithValue("statusCode", HttpStatus.UNPROCESSABLE_ENTITY);

            verify(bookReservationRepository).countAllByBookIdAndUserId(1,1);
        }

        @Test
        void postNewBookReservationMaxReservations() {
            when(bookReservationRepository.countAllByBookIdAndUserId(1,1)).thenReturn(0);
            when(bookReservationService.countBookReservationByLibrary(1, 1)).thenReturn(2);
            when(bookCopyRepository.countByLibraryIdAndBookId(1, 1)).thenReturn(1);

            ResponseEntity result = reservationController.postNewBookReservation(1,1,1);

            assertThat(result).hasFieldOrPropertyWithValue("statusCode", HttpStatus.UNPROCESSABLE_ENTITY);

            verify(bookReservationRepository).countAllByBookIdAndUserId(1,1);
            verify(bookReservationService).countBookReservationByLibrary(1, 1);
            verify(bookCopyRepository).countByLibraryIdAndBookId(1, 1);
        }

        @Test
        void postNewBookReservationWithoutReservation() {
            when(bookReservationRepository.countAllByBookIdAndUserId(1,1)).thenReturn(0);
            when(bookReservationRepository.save(any())).thenReturn(null);
            when(bookReservationService.countBookReservationByLibrary(1, 1)).thenReturn(2);
            when(bookCopyRepository.countByLibraryIdAndBookId(1, 1)).thenReturn(2);

            ResponseEntity result = reservationController.postNewBookReservation(1,1,1);

            assertThat(result).hasFieldOrPropertyWithValue("statusCode", HttpStatus.OK);

            verify(bookReservationRepository).countAllByBookIdAndUserId(1,1);
            verify(bookReservationRepository).save(any());
            verify(bookReservationService).countBookReservationByLibrary(1, 1);
            verify(bookCopyRepository).countByLibraryIdAndBookId(1, 1);
        }
    }

    @Test
    void cancelBookReservation() {
        doNothing().when(bookReservationRepository).deleteById(1L);

        ResponseEntity result = reservationController.cancelBookReservation(1);

        assertThat(result).hasFieldOrPropertyWithValue("statusCode", HttpStatus.OK);

        verify(bookReservationRepository).deleteById(1L);
    }
}