package com.citylib.library.controller;

import com.citylib.library.config.ServiceRoutes;
import com.citylib.library.model.BookCopy;
import com.citylib.library.model.BookReservation;
import com.citylib.library.model.RentedCopy;
import com.citylib.library.repository.BookCopyRepository;
import com.citylib.library.repository.BookReservationRepository;
import com.citylib.library.repository.RentedCopyRepository;
import com.citylib.library.service.BookReservationService;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

class BatchOpsControllerTest {

    @Mock
    private BookCopyRepository bookCopyRepository;
    @Mock
    private RentedCopyRepository rentedCopyRepository;
    @Mock
    private BookReservationRepository bookReservationRepository;
    @Mock
    private BookReservationService bookReservationService;
    @Mock
    private ServiceRoutes serviceRoutes;
    @Mock
    private RestTemplate restTemplate;

    @Mock
    BookCopy bookCopy;

    @Spy
    @InjectMocks
    private BatchOpsController batchOpsController;

    @BeforeEach
    void setUp() {
        initMocks(this);
        batchOpsController.setRentTime(5);
    }

    @Test
    void getPastDueDate() {
        List<RentedCopy> rentedCopyList = new ArrayList<>();
        RentedCopy rentedCopy = new RentedCopy();
        rentedCopy.setId(1);
        rentedCopy.setUserId(1);
        rentedCopy.setStartDate(LocalDate.now());
        rentedCopy.setExtended(true);
        rentedCopy.setReturned(false);
        rentedCopy.setBookCopy(bookCopy);
        rentedCopyList.add(rentedCopy);

        when(rentedCopyRepository.findPastDueDate(LocalDate.now().minusDays(6), false)).thenReturn(rentedCopyList);
        when(rentedCopyRepository.findPastDueDate(LocalDate.now().minusDays(12), true)).thenReturn(rentedCopyList);

        ResponseEntity<List<RentedCopy>> result = batchOpsController.getPastDueDate();

        rentedCopyList.add(rentedCopy);

        assertThat(result).hasFieldOrPropertyWithValue("body", rentedCopyList).hasFieldOrPropertyWithValue("statusCode", HttpStatus.OK);

        verify(rentedCopyRepository).findPastDueDate(LocalDate.now().minusDays(6), false);
        verify(rentedCopyRepository).findPastDueDate(LocalDate.now().minusDays(12), true);
    }

    @Test
    void getReservationsOnStandby() {
        BookReservation bookReservation = new BookReservation();
        BookReservation bookReservationNotified = new BookReservation();

        bookReservation.setId(1);
        bookReservation.setUserId(1);
        bookReservation.setBookId(1);
        bookReservation.setLibraryId(1);
        bookReservation.setReservedTime(LocalDateTime.now());
        bookReservation.setNotified(false);

        bookReservationNotified.setId(2);
        bookReservationNotified.setUserId(2);
        bookReservationNotified.setBookId(1);
        bookReservationNotified.setLibraryId(1);
        bookReservationNotified.setReservedTime(LocalDateTime.now().minusDays(5));
        bookReservationNotified.setNotifiedTime(LocalDateTime.now().minusDays(2));
        bookReservationNotified.setNotified(false);

        BookReservation bookReservationReadyToNotify = bookReservation;
        bookReservationReadyToNotify.setReadyToNotify(true);

        List<BookReservation> bookReservationNotifiedList = new ArrayList<>();
        bookReservationNotifiedList.add(bookReservationNotified);

        List<BookReservation> bookReservationList = new ArrayList<>();
        bookReservationList.add(bookReservation);

        when(bookReservationRepository.findAllByNotifiedTrue()).thenReturn(bookReservationNotifiedList);
        when(serviceRoutes.sendReservationUnavailableEmail(2L, 1L)).thenReturn("reservationPath");
        when(restTemplate.getForEntity("reservationPath", String.class)).thenReturn(new ResponseEntity<>("true", HttpStatus.OK));
        when(bookReservationRepository.findAllByNotifiedFalse()).thenReturn(bookReservationList);
        doReturn(2).when(batchOpsController).availableCopies(1,1);
        when(bookReservationRepository.countAllByBookIdAndLibraryIdAndNotifiedAndReadyToNotify(1, 1, false, true)).thenReturn(1);
        when(bookReservationRepository.save(bookReservation)).thenReturn(bookReservationReadyToNotify);
        when(bookReservationRepository.save(bookReservation)).thenReturn(bookReservation);

        ResponseEntity<List<BookReservation>> result = batchOpsController.getReservationsOnStandby();

        assertThat(result).hasFieldOrPropertyWithValue("body", bookReservationList).hasFieldOrPropertyWithValue("statusCode", HttpStatus.OK);

        verify(bookReservationRepository).findAllByNotifiedTrue();
        verify(serviceRoutes).sendReservationUnavailableEmail(2L, 1L);
        verify(restTemplate).getForEntity("reservationPath", String.class);
        verify(bookReservationRepository).findAllByNotifiedFalse();
        verify(batchOpsController).availableCopies(1,1);
        verify(bookReservationRepository).countAllByBookIdAndLibraryIdAndNotifiedAndReadyToNotify(1, 1, false, true);
        verify(bookReservationRepository, times(2)).save(bookReservation);

//        logger.info("List of reservations to alert requested.");
//        List<BookReservation >reservationAlertList = new ArrayList<>();
//        List<BookReservation> sentBookReservationList = bookReservationRepository.findAllByNotifiedTrue();
//
//        int cancelledReservations = 0;
//        for (BookReservation sentBookReservation : sentBookReservationList) {
//            if (sentBookReservation.getNotifiedTime().plusDays(2).plusHours(3).isAfter(LocalDateTime.now())) {
//                String uri = serviceRoutes.sendReservationUnavailableEmail(sentBookReservation.getUserId(), sentBookReservation.getBookId());
//                ResponseEntity<String> response = restTemplate.getForEntity(uri, String.class);
//                if(response.getBody().equals("true")) {
//                    bookReservationRepository.deleteById(sentBookReservation.getId());
//                    cancelledReservations++;
//                }
//            }
//        }
//        logger.info(cancelledReservations + " reservations reached their end and were cancelled");
//
//        List<BookReservation> bookReservationList = bookReservationRepository.findAllByNotifiedFalse();
//        logger.info("There are currently " + bookReservationList.size() + " in waiting");
//
//        int countReservationsToBeSent = 0;
//        for(BookReservation eachReservation : bookReservationList) {
//            int numAvailable = availableCopies(eachReservation.getBookId(), eachReservation.getLibraryId());
//            if(numAvailable > 0) {
//                int numActiveReservations = bookReservationRepository.countAllByBookIdAndLibraryIdAndNotified(eachReservation.getBookId(), eachReservation.getLibraryId(), true);
//                int numReservationReadyToBeSent = bookReservationRepository.countAllByBookIdAndLibraryIdAndNotifiedAndReadyToNotify(eachReservation.getBookId(), eachReservation.getLibraryId(), false, true);
//                logger.info(numActiveReservations + " reservations active");
//                logger.info(numReservationReadyToBeSent + " reservations ready to be sent");
//                if((numActiveReservations + numReservationReadyToBeSent) < numAvailable) {
//                    eachReservation.setReadyToNotify(true);
//                    bookReservationRepository.save(eachReservation);
//                    reservationAlertList.add(eachReservation);
//                    countReservationsToBeSent++;
//                }
//            }
//        }
//        logger.info(countReservationsToBeSent + " reservations are ready to be informed");
//
//        // Resets ready to notify, so that it doesn't block future requests in case of errors
//        for (BookReservation bookReservation : reservationAlertList) {
//            bookReservation.setReadyToNotify(false);
//            bookReservationRepository.save(bookReservation);
//        }
//
//        return new ResponseEntity<>(reservationAlertList, HttpStatus.OK);
    }

    @Test
    void getReservationNotified() {
        BookReservation bookReservation = new BookReservation();
        bookReservation.setId(1);
        bookReservation.setUserId(1);
        bookReservation.setBookId(1);
        bookReservation.setLibraryId(1);
        bookReservation.setReservedTime(LocalDateTime.now());
        bookReservation.setNotified(false);

        ArgumentCaptor<BookReservation> bookReservationArgumentCaptor = ArgumentCaptor.forClass(BookReservation.class);

        when(bookReservationRepository.findAllById(1)).thenReturn(bookReservation);
        when(bookReservationService.save(bookReservationArgumentCaptor.capture())).thenReturn(bookReservation);

        ResponseEntity result = batchOpsController.getReservationNotified(1);

        assertThat(result).hasFieldOrPropertyWithValue("statusCode", HttpStatus.OK);
        assertThat(bookReservationArgumentCaptor.getValue().isNotified()).isTrue();

        verify(bookReservationRepository).findAllById(1);
        verify(bookReservationService).save(bookReservationArgumentCaptor.getValue());
    }

    @Test
    void availableCopies() {
        when(bookCopyRepository.countByLibraryIdAndBookId(1,1)).thenReturn(3);
        when(rentedCopyRepository.countAllByBookCopy_BookIdAndReturned(1, false)).thenReturn(2);

        int result = batchOpsController.availableCopies(1,1);

        assertThat(result).isEqualTo(1);

        verify(bookCopyRepository).countByLibraryIdAndBookId(1,1);
        verify(rentedCopyRepository).countAllByBookCopy_BookIdAndReturned(1, false);
    }
}