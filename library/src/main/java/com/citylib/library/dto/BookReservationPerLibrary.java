package com.citylib.library.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@Getter @Setter @ToString
public class BookReservationPerLibrary {
    long bookId;
    long libraryId;
    int reservationCount;
}
