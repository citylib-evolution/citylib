package com.citylib.library.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Transient;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BookReservation {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private long userId;

    private long bookId;

    private long libraryId;

    private LocalDateTime reservedTime;

    private LocalDateTime notifiedTime;

    private boolean notified;

    private boolean readyToNotify;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Transient
    private Integer placement;
}
