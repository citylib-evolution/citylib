package com.citylib.library.model;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Data
public class RentedCopy {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private long userId;
    private LocalDate startDate;
    private boolean extended;
    private boolean returned;

    @ManyToOne(fetch = FetchType.EAGER)
    private BookCopy bookCopy;
}
