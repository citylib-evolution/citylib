package com.citylib.library.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;

@Getter
@PropertySource("classpath:application.properties")
public class ServiceRoutes {
    @Value("${service.emailbatch}")
    private String emailBatch;

    public String sendReservationUnavailableEmail(long userId, long bookId) {
        return emailBatch + "reservationSupressed?userId=" + userId + "&bookId=" + bookId;
    }
}
