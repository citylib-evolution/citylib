package com.citylib.library.controller;

import com.citylib.library.dto.BookReservationPerLibrary;
import com.citylib.library.model.BookReservation;
import com.citylib.library.model.Library;
import com.citylib.library.repository.BookCopyRepository;
import com.citylib.library.repository.BookReservationRepository;
import com.citylib.library.repository.LibraryRepository;
import com.citylib.library.service.BookReservationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/library/reservation")
public class ReservationController {

    private final LibraryRepository libraryRepository;
    private final BookReservationService bookReservationService;
    private final BookReservationRepository bookReservationRepository;
    private final BookCopyRepository bookCopyRepository;

    @Autowired
    public ReservationController(LibraryRepository libraryRepository, BookReservationService bookReservationService,
                                 BookReservationRepository bookReservationRepository, BookCopyRepository bookCopyRepository) {
        this.libraryRepository = libraryRepository;
        this.bookReservationService = bookReservationService;
        this.bookReservationRepository = bookReservationRepository;
        this.bookCopyRepository = bookCopyRepository;
    }

    @GetMapping(value = "userHasReservation", produces = "application/json")
    public ResponseEntity userHasReservation(@RequestParam("userId") long userId, @RequestParam("bookId") long bookId) {
        int userReservationCount = bookReservationRepository.countAllByBookIdAndUserId(bookId, userId);
        if(userReservationCount > 0) {
            return new ResponseEntity("true", HttpStatus.OK);
        } else {
            return new ResponseEntity("false", HttpStatus.OK);
        }
    }

    @GetMapping(value = "getUserBookReservations", produces = "application/json")
    public ResponseEntity getUserBookReservation(@RequestParam("userId") long userId) {
        List<BookReservation> reservationList = bookReservationRepository.findAllByUserId(userId);
        if(!reservationList.isEmpty()) {
            for (BookReservation reservation : reservationList) {
                reservation.setPlacement(bookReservationService.getReservationPlacement(reservation.getBookId(), reservation.getLibraryId(), reservation.getUserId()));
            }
        }
        return new ResponseEntity(reservationList, HttpStatus.OK);
    }

    @GetMapping(value = "bookReservationsCount", produces = "application/json")
    public ResponseEntity getBookReservations(@RequestParam("bookId") long bookId) {
        List<BookReservationPerLibrary> bookReservationCount = new ArrayList<>();
        List<Library> libraryList = libraryRepository.findAll();
        for (Library library : libraryList) {
            int count = bookReservationService.countBookReservationByLibrary(bookId, library.getId());
            BookReservationPerLibrary reservationPerLibrary = new BookReservationPerLibrary(bookId, library.getId(), count);
            bookReservationCount.add(reservationPerLibrary);
        }
        return new ResponseEntity<>(bookReservationCount, HttpStatus.OK);
    }

    @GetMapping(value = "newBookReservation", produces = "application/json")
    public ResponseEntity postNewBookReservation(
            @RequestParam("bookId") long bookId,
            @RequestParam("libraryId") long libraryId,
            @RequestParam("userId") long userId
    ) {

        // If user already has a reservation for given book, error is returned
        if(bookReservationRepository.countAllByBookIdAndUserId(bookId, userId) > 0) {
            return new ResponseEntity("{error: 'User already has a reservation for the given book'}", HttpStatus.UNPROCESSABLE_ENTITY);
        }

        BookReservation bookReservation = BookReservation.builder()
                .bookId(bookId)
                .libraryId(libraryId)
                .userId(userId)
                .notified(false)
                .readyToNotify(false)
                .reservedTime(LocalDateTime.now())
                .notifiedTime(null)
                .build();

        int existingReservations = bookReservationService.countBookReservationByLibrary(bookId, libraryId);
        int existingCopies = bookCopyRepository.countByLibraryIdAndBookId(libraryId, bookId);

        System.out.println("Existing reservations : " + existingReservations);
        System.out.println("Existing copies : " + existingCopies);

        if (existingCopies * 2 > existingReservations) {
            try {
                bookReservationRepository.save(bookReservation);
            } catch (Exception e) {
                return new ResponseEntity("{error: 'An error ocurred while processing reservation'}", HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } else {
            return new ResponseEntity("{error: 'The maximum number of reservations has already been reached'}", HttpStatus.UNPROCESSABLE_ENTITY);
        }

        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping(value = "cancelBookReservation", produces = "application/json")
    public ResponseEntity cancelBookReservation(@RequestParam("reservationId") long reservationId) {
        try {
            bookReservationRepository.deleteById(reservationId);
        } catch (Exception e) {
            return new ResponseEntity("{error: 'An error ocurred while processing reservation'}", HttpStatus.UNPROCESSABLE_ENTITY);
        }

        return new ResponseEntity(HttpStatus.OK);
    }
}
