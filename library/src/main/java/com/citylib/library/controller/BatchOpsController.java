package com.citylib.library.controller;

import com.citylib.library.config.ServiceRoutes;
import com.citylib.library.model.BookReservation;
import com.citylib.library.model.RentedCopy;
import com.citylib.library.repository.BookCopyRepository;
import com.citylib.library.repository.BookReservationRepository;
import com.citylib.library.repository.RentedCopyRepository;
import com.citylib.library.service.BookReservationService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/library/batchOps")
public class BatchOpsController {

    private final BookCopyRepository bookCopyRepository;
    private final RentedCopyRepository rentedCopyRepository;
    private final BookReservationRepository bookReservationRepository;
    private final BookReservationService bookReservationService;
    private final ServiceRoutes serviceRoutes;
    private final RestTemplate restTemplate;

    @Value("${config.options.rentTime}")
    private int rentTime;

    Logger logger = LoggerFactory.getLogger(BatchOpsController.class);
    private ObjectMapper objectMapper = new ObjectMapper();


    @Autowired
    public BatchOpsController(BookCopyRepository bookCopyRepository, RentedCopyRepository rentedCopyRepository,
                              BookReservationRepository bookReservationRepository,
                              BookReservationService bookReservationService, ServiceRoutes serviceRoutes,
                              RestTemplate restTemplate) {
        this.bookCopyRepository = bookCopyRepository;
        this.rentedCopyRepository = rentedCopyRepository;
        this.bookReservationRepository = bookReservationRepository;
        this.bookReservationService = bookReservationService;
        this.serviceRoutes = serviceRoutes;
        this.restTemplate = restTemplate;
    }

    @GetMapping("/pastDueDate")
    public ResponseEntity<List<RentedCopy>> getPastDueDate() {
        LocalDate nonExtendedDate = LocalDate.now().minusDays(rentTime + 1);
        LocalDate extendedDate = LocalDate.now().minusDays((rentTime +1) * 2);

        List<RentedCopy> dueLateList = rentedCopyRepository.findPastDueDate(nonExtendedDate, false);
        dueLateList.addAll(rentedCopyRepository.findPastDueDate(extendedDate, true));

        return new ResponseEntity<>(dueLateList, HttpStatus.OK);
    }

    @GetMapping("/reservationAlertsToBeSent")
    public ResponseEntity<List<BookReservation>> getReservationsOnStandby() {
        logger.info("List of reservations to alert requested.");
        List<BookReservation >reservationAlertList = new ArrayList<>();
        List<BookReservation> sentBookReservationList = bookReservationRepository.findAllByNotifiedTrue();

        int cancelledReservations = 0;
        for (BookReservation sentBookReservation : sentBookReservationList) {
            if (LocalDateTime.now().isAfter(sentBookReservation.getNotifiedTime().plusDays(2).minusHours(2))) {
                String uri = serviceRoutes.sendReservationUnavailableEmail(sentBookReservation.getUserId(), sentBookReservation.getBookId());
                ResponseEntity<String> response = restTemplate.getForEntity(uri, String.class);
                if(response.getBody().equals("true")) {
                    bookReservationRepository.deleteById(sentBookReservation.getId());
                    cancelledReservations++;
                }
            }
        }
        logger.info(cancelledReservations + " reservations reached their end and were cancelled");

        List<BookReservation> bookReservationList = bookReservationRepository.findAllByNotifiedFalse();
        logger.info("There are currently " + bookReservationList.size() + " in waiting");

        int countReservationsToBeSent = 0;
        for(BookReservation eachReservation : bookReservationList) {
            int numAvailable = availableCopies(eachReservation.getBookId(), eachReservation.getLibraryId());
            if(numAvailable > 0) {
                int numActiveReservations = bookReservationRepository.countAllByBookIdAndLibraryIdAndNotified(eachReservation.getBookId(), eachReservation.getLibraryId(), true);
                int numReservationReadyToBeSent = bookReservationRepository.countAllByBookIdAndLibraryIdAndNotifiedAndReadyToNotify(eachReservation.getBookId(), eachReservation.getLibraryId(), false, true);
                logger.info(numActiveReservations + " reservations active");
                logger.info(numReservationReadyToBeSent + " reservations ready to be sent");
                if((numActiveReservations + numReservationReadyToBeSent) < numAvailable) {
                    eachReservation.setReadyToNotify(true);
                    bookReservationRepository.save(eachReservation);
                    reservationAlertList.add(eachReservation);
                    countReservationsToBeSent++;
                }
            }
        }
        logger.info(countReservationsToBeSent + " reservations are ready to be informed");

        // Resets ready to notify, so that it doesn't block future requests in case of errors
        for (BookReservation bookReservation : reservationAlertList) {
            bookReservation.setReadyToNotify(false);
            bookReservationRepository.save(bookReservation);
        }

        return new ResponseEntity<>(reservationAlertList, HttpStatus.OK);
    }

    @GetMapping(value = "/reservationNotified", produces = "application/json")
    public ResponseEntity getReservationNotified(@RequestParam("reservationId") long reservationId) {
        BookReservation reservation = bookReservationRepository.findAllById(reservationId);
        reservation.setNotified(true);
        reservation.setNotifiedTime(LocalDateTime.now());
        BookReservation result = bookReservationService.save(reservation);
        if(result != null) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    int availableCopies(long bookId, long libraryId) {
        int numCopies = bookCopyRepository.countByLibraryIdAndBookId(libraryId, bookId);
        int numRentedCopies = rentedCopyRepository.countAllByBookCopy_BookIdAndReturned(bookId, false);
        logger.info("[ " + numRentedCopies + " / " + numCopies + " ] are available");
        return numCopies - numRentedCopies;
    }

    public int getRentTime() {
        return rentTime;
    }

    public void setRentTime(int rentTime) {
        this.rentTime = rentTime;
    }
}
