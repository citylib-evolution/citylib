package com.citylib.library.repository;

import com.citylib.library.model.BookCopy;
import com.citylib.library.model.BookReservation;
import com.citylib.library.model.Library;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BookReservationRepository extends JpaRepository<BookReservation, Long> {

    /**
     * Provides number of reservations for a given Book in a Library
     * @param bookId id of book associated with the reservations
     * @param libraryId id of library containing the reservation
     * @return reservation count
     */
    int countAllByLibraryIdAndBookId(long bookId, long libraryId);

    /**
     * Provides reservations for a given Book in a Library
     * @param bookId id of book associated with the reservations
     * @param libraryId id of library containing the reservation
     * @return Reservation List
     */
    List<BookReservation> findAllByBookIdAndLibraryIdOrderByReservedTimeAsc(long bookId, long libraryId);

    /**
     * Returns the list of reservations for a given user
     * @param userId id of user
     * @return list of reservations for the user
     */
    List<BookReservation> findAllByUserId(long userId);

    /**
     * Counts number of user reservations for a given book
     * @param bookId id of book
     * @param userId id of user
     * @return count of reservations
     */
    int countAllByBookIdAndUserId(long bookId, long userId);

    /**
     * Retrieves user reservation for a given book
     * @param bookId id of book
     * @param userId id of user
     * @return Reservation
     */
    BookReservation findByBookIdAndUserId(long bookId, long userId);

    List<BookReservation> findAllByNotifiedFalse();

    List<BookReservation> findAllByNotifiedTrue();

    int countAllByBookIdAndLibraryIdAndNotified(long bookId, long libraryId, boolean notified);
    int countAllByBookIdAndLibraryIdAndNotifiedAndReadyToNotify(long bookId, long libraryId, boolean notified, boolean readyToNotify);

    BookReservation findAllById(long reservationId);
}
