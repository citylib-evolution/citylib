package com.citylib.library.service;

import com.citylib.library.model.BookReservation;
import com.citylib.library.repository.BookReservationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.awt.print.Book;
import java.util.List;

@Service
public class BookReservationService {

    private final BookReservationRepository bookReservationRepository;

    @Autowired
    public BookReservationService(BookReservationRepository bookReservationRepository) {
        this.bookReservationRepository = bookReservationRepository;
    }

    public List<BookReservation> getBookReservationByLibrary(long bookId, long libraryId) {
        return bookReservationRepository.findAllByBookIdAndLibraryIdOrderByReservedTimeAsc(bookId, libraryId);
    }

    public int countBookReservationByLibrary(long bookId, long libraryId) {
        return bookReservationRepository.countAllByLibraryIdAndBookId(libraryId, bookId);
    }

    public int getReservationPlacement(long bookId, long libraryId, long userId) {
        List<BookReservation> reservationList = getBookReservationByLibrary(bookId, libraryId);
        int i = 0;
        for(BookReservation reservation : reservationList) {
            if(reservation.getUserId() == userId) {
                break;
            }
            i++;
        }
        return i;
    }

    public BookReservation save(BookReservation reservation) {
        return bookReservationRepository.save(reservation);
    }
}
