package com.citylib.library.service;

import com.citylib.library.dto.LibraryWithCopies;
import com.citylib.library.model.BookCopy;
import com.citylib.library.model.Library;
import com.citylib.library.repository.BookCopyRepository;
import com.citylib.library.repository.LibraryRepository;
import com.citylib.library.repository.RentedCopyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BookCopyService {
    private final BookCopyRepository bookCopyRepository;
    private final LibraryRepository libraryRepository;
    private final RentedCopyRepository rentedCopyRepository;

    @Autowired
    public BookCopyService(BookCopyRepository bookCopyRepository, LibraryRepository libraryRepository, RentedCopyRepository rentedCopyRepository) {
        this.bookCopyRepository = bookCopyRepository;
        this.libraryRepository = libraryRepository;
        this.rentedCopyRepository = rentedCopyRepository;
    }

    public List<LibraryWithCopies> findAllCopiesByBookId(long id) {
        LibraryWithCopies libraryWithCopies;
        List<LibraryWithCopies> copyList = new ArrayList<>();
        List<Library> libraryList = libraryRepository.findAll();
        for (Library library : libraryList) {
            List<BookCopy> copiesByLibrary = bookCopyRepository.findAllByLibraryAndBookId(library, id);

            libraryWithCopies = new LibraryWithCopies();
            libraryWithCopies.setId(library.getId());
            libraryWithCopies.setName(library.getName());
            libraryWithCopies.setCopyCount(bookCopyRepository.countByLibraryIdAndBookId(library.getId(), id));
            libraryWithCopies.setAvailableCount(libraryWithCopies.getCopyCount());
            for(BookCopy bookCopy : copiesByLibrary) {
                //Checks if copy is available, reduces available count if not
                if(!copyAvailable(bookCopy.getId())) {
                    libraryWithCopies.setAvailableCount(libraryWithCopies.getAvailableCount() - 1);
                }
            }
            copyList.add(libraryWithCopies);
        }
        return copyList;
    }

    public List<BookCopy> findAvailableCopy(long bookId, long libraryId) {
        Library lib = libraryRepository.findById(libraryId);
        List<BookCopy> availableCopyList = new ArrayList<>();
        List<BookCopy> copyList = bookCopyRepository.findAllByLibraryAndBookId(lib, bookId);
        for (BookCopy bookCopy : copyList) {
            if(copyAvailable(bookCopy.getId())) {
                availableCopyList.add(bookCopy);
            }
        }
        return availableCopyList;
    }

    public boolean copyAvailable(long copyId){
        BookCopy bookCopy = bookCopyRepository.findById(copyId);
        if(rentedCopyRepository.countByBookCopyAndReturned(bookCopy, false) == 0) {
            return true;
        } else {
            return false;
        }
    };
}
