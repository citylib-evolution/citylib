package com.citylib.books.dao;

import com.citylib.books.model.Author;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorRepository extends JpaRepository<Author, Long> {
    Author findById(long authorId);
}
