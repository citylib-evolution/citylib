package com.citylib.books.dao;

import com.citylib.books.model.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface BookRepository extends JpaRepository<Book, Long> {
    Book findById(long bookId);

    @Query("select distinct b from Book b, Author a, Publisher p " +
            "where lower(b.title) like lower(CONCAT('%',:search,'%')) or " +
            "lower(concat(b.author.name, ' ', b.author.surname)) like lower(CONCAT('%',:search,'%')) or " +
            "lower(b.publisher.name) like lower(CONCAT('%',:search,'%')) and " +
            "b.author = a.id and b.publisher = p.id")
    Page<Book> findAllWithSearch(String search, Pageable page);
}
