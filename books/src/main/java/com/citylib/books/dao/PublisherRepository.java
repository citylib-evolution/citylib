package com.citylib.books.dao;

import com.citylib.books.model.Author;
import com.citylib.books.model.Publisher;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PublisherRepository extends JpaRepository<Publisher, Long> {
    Publisher findById(long publisherId);
}
