package com.citylib.books.controller;

import com.citylib.books.dao.AuthorRepository;
import com.citylib.books.dao.BookRepository;
import com.citylib.books.dao.PublisherRepository;
import com.citylib.books.model.Author;
import com.citylib.books.model.Book;
import com.citylib.books.model.Publisher;
import com.citylib.books.service.impl.BookServiceImpl;
import javassist.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@RestController
@RequestMapping("/api/books")
public class BookController {
    private final BookServiceImpl bookService;
    private final BookRepository bookRepository;
    private final AuthorRepository authorRepository;
    private final PublisherRepository publisherRepository;

    Logger logger = LoggerFactory.getLogger(BookController.class);

    @Autowired
    public BookController(BookServiceImpl bookService, BookRepository bookRepository, AuthorRepository authorRepository, PublisherRepository publisherRepository) {
        this.bookService = bookService;
        this.bookRepository = bookRepository;
        this.authorRepository = authorRepository;
        this.publisherRepository = publisherRepository;
    }

    @GetMapping(value = "", produces = "application/json")
    public Page<Book> getBookList(
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "size", defaultValue = "10") Integer size,
            @RequestParam(value = "sort", defaultValue = "title") String sort,
            @RequestParam(value = "search", defaultValue = "", required = false) String search) {
        Pageable pageable = PageRequest.of(page - 1, size, Sort.by(sort));
        Page<Book> bookPage = bookRepository.findAllWithSearch(search, pageable);
        return bookPage;
    }

    @GetMapping(value = "/{id}", produces = "application/json")
    public ResponseEntity getBook(@PathVariable("id") long id) {
        Book book;
        try {
            book = bookService.find(id);
        } catch (NotFoundException e) {
            String message = "Book with id " + id + " does not exist";
            logger.info(message);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(book, HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity postBook(@RequestParam("title") String title,
                                   @RequestParam("description") String description,
                                   @RequestParam("availableQty") int availableQty,
                                   @RequestParam("releaseDate") String releaseDate,
                                   @RequestParam("authorId") long authorId,
                                   @RequestParam("publisherId") long publisherId) {
        Publisher publisher = publisherRepository.findById(publisherId);
        Author author = authorRepository.findById(authorId);
        Book book = new Book();
        book.setTitle(title);
        book.setDescription(description);
        book.setAvailableQty(availableQty);
        book.setReleaseDate(LocalDate.parse(releaseDate, DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        book.setAuthor(author);
        book.setPublisher(publisher);
        bookRepository.save(book);
        logger.info("New book was added with id " + book.getId());
        return new ResponseEntity<>("", HttpStatus.OK);
    }

    @PutMapping("/{bookId}")
    public ResponseEntity putBook(@PathVariable("bookId") long bookId,
                                    @RequestParam(value = "title",required = false, defaultValue = "") String title,
                                    @RequestParam(value = "description", required = false, defaultValue = "") String description,
                                    @RequestParam(value = "availableQty", required = false, defaultValue = "0") int availableQty,
                                    @RequestParam(value = "releaseDate", required = false, defaultValue = "") String releaseDate,
                                    @RequestParam(value = "authorId", required = false, defaultValue = "0") long authorId,
                                    @RequestParam(value = "publisherId", required = false, defaultValue = "0") long publisherId) {
        Book book = bookRepository.findById(bookId);
        if(!title.equals("")) {
            book.setTitle(title);
        }
        if(!description.equals("")) {
            book.setDescription(description);
        }
        if(availableQty != 0){
            book.setAvailableQty(availableQty);
        }
        if(!releaseDate.equals("")) {
            book.setReleaseDate(LocalDate.parse(releaseDate, DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        }
        if(authorId != 0) {
            book.setAuthor(authorRepository.findById(authorId));
        }
        if(publisherId != 0) {
            book.setPublisher(publisherRepository.findById(publisherId));
        }
        bookRepository.save(book);
        logger.info("Book with id " + book.getId() + " has been modified");
        return new ResponseEntity<>("", HttpStatus.OK);
    }

    @DeleteMapping("/{bookId}")
    public ResponseEntity deleteBook(@PathVariable("bookId") long bookId){
        try {
            Book book = bookRepository.findById(bookId);
            bookRepository.delete(book);
            logger.info("Book with id " + book.getId() + " has been deleted");
        } catch (DataIntegrityViolationException e) {
            return new ResponseEntity<>("{error: 'An error occurred while deleting book'}", HttpStatus.UNPROCESSABLE_ENTITY);
        }
        return new ResponseEntity<>("", HttpStatus.OK);
    }
}
