package com.citylib.books.controller;

import com.citylib.books.dao.PublisherRepository;
import com.citylib.books.model.Publisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/publisher")
public class PublisherController {
    private final PublisherRepository publisherRepository;

    Logger logger = LoggerFactory.getLogger(PublisherController.class);

    @Autowired
    public PublisherController(PublisherRepository publisherRepository) {
        this.publisherRepository = publisherRepository;
    }

    @GetMapping("/{publisherId}")
    public ResponseEntity getPublisher(@PathVariable("publisherId") long publisherId) {
        Publisher publisher = publisherRepository.findById(publisherId);
        return new ResponseEntity<>(publisher, HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity postPublisher(@RequestParam("name") String name) {
        Publisher publisher = new Publisher();
        publisher.setName(name);
        publisherRepository.save(publisher);
        logger.info("New publisher added with id " + publisher.getId());
        return new ResponseEntity<>("", HttpStatus.OK);
    }

    @PutMapping("/{publisherId}")
    public ResponseEntity putPublisher(@PathVariable("publisherId") long publisherId,
                                    @RequestParam(value = "name", required = false, defaultValue = "") String name) {
        Publisher publisher = publisherRepository.findById(publisherId);
        if(!name.equals("")) {
            publisher.setName(name);
        }
        publisherRepository.save(publisher);
        logger.info("Publisher with id " + publisher.getId() + " has been modified");
        return new ResponseEntity<>("", HttpStatus.OK);
    }

    @DeleteMapping("/{publisherId}")
    public ResponseEntity deletePublisher(@PathVariable("publisherId") long publisherId){
        try {
            Publisher publisher = publisherRepository.findById(publisherId);
            publisherRepository.delete(publisher);
            logger.info("Publisher with id " + publisher.getId() + " has been deleted");
        } catch (DataIntegrityViolationException e) {
            return new ResponseEntity<>("{error: 'This publisher is still being used by book entities.'}", HttpStatus.UNPROCESSABLE_ENTITY);
        }
        return new ResponseEntity<>("", HttpStatus.OK);
    }
}
