package com.citylib.books.service;

import com.citylib.books.model.Book;

public interface BookService extends GenericService<Book> {
}
