package com.citylib.books.controller;

import com.citylib.books.dao.AuthorRepository;
import com.citylib.books.dao.BookRepository;
import com.citylib.books.dao.PublisherRepository;
import com.citylib.books.model.Author;
import com.citylib.books.model.Book;
import com.citylib.books.model.Publisher;
import com.citylib.books.service.impl.BookServiceImpl;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import javassist.NotFoundException;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

class BookControllerTest {
    @Mock
    private BookServiceImpl bookService;
    @Mock
    private BookRepository bookRepository;
    @Mock
    private AuthorRepository authorRepository;
    @Mock
    private PublisherRepository publisherRepository;
    @Mock
    private Page<Book> page;

    Book book;
    Author author;
    Publisher publisher;

    @Spy
    @InjectMocks
    private BookController bookController;

    @BeforeEach
    void setUp() {
        initMocks(this);

        author = new Author();
        author.setName("Jane");
        author.setSurname("Doe");
        author.setBirthDate(LocalDate.parse("22/11/1992", DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        author.setId(0);

        publisher = new Publisher();
        publisher.setId(0);
        publisher.setName("Books & Co.");

        book = new Book();
        book.setId(0);
        book.setTitle("RandBook");
        book.setDescription("Ramble... Ramble... Ramble on!");
        book.setAvailableQty(1);
        book.setReleaseDate(LocalDate.parse("22/11/1992", DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        book.setAuthor(author);
        book.setPublisher(publisher);
    }

    @Test
    void getBookList() throws IOException {
        when(bookRepository.findAllWithSearch(anyString(), any())).thenReturn(page);
        assertThat(bookController.getBookList(1, 2, "title", "")).isEqualTo(page);
        verify(bookRepository).findAllWithSearch(anyString(), any());
    }


    @Test
    void getBook() throws NotFoundException {
        when(bookService.find(0)).thenReturn(book);
        assertThat(bookController.getBook(0)).hasFieldOrPropertyWithValue("body", book);
        verify(bookService).find(0);

    }

    @Test
    void postBook() {
        when(authorRepository.findById(0)).thenReturn(author);
        when(publisherRepository.findById(0)).thenReturn(publisher);
        when(bookRepository.save(book)).thenReturn(book);
        assertThat(bookController.postBook(book.getTitle(), book.getDescription(), book.getAvailableQty(),
                book.getReleaseDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")),
                book.getAuthor().getId(), book.getPublisher().getId()))
                .hasFieldOrPropertyWithValue("body", "").hasFieldOrPropertyWithValue("statusCode", HttpStatus.OK);
        verify(bookRepository).save(any());
    }

    @Test
    void putAuthor() {
        Book returnBook = book;
        returnBook.setTitle("Another Title");
        returnBook.setDescription("Proper description");
        returnBook.setAvailableQty(5);

        when(bookRepository.findById(0)).thenReturn(book);
        when(bookRepository.save(returnBook)).thenReturn(returnBook);

        assertThat(bookController.putBook(0, "Another Title", "Proper description", 5, "", 0, 0))
                .hasFieldOrPropertyWithValue("body", "").hasFieldOrPropertyWithValue("statusCode", HttpStatus.OK);
        verify(bookRepository).findById(0);
        verify(bookRepository).save(returnBook);
    }

    @Test
    void deleteAuthor() {
        when(bookRepository.findById(0)).thenReturn(book);
        doNothing().when(bookRepository).delete(book);

        assertThat(bookController.deleteBook(0))
                .hasFieldOrPropertyWithValue("body", "").hasFieldOrPropertyWithValue("statusCode", HttpStatus.OK);

        verify(bookRepository).findById(0);
        verify(bookRepository).delete(book);
    }
}