package com.citylib.books.controller;

import com.citylib.books.dao.AuthorRepository;
import com.citylib.books.model.Author;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

class AuthorControllerTest {
    @Mock
    private AuthorRepository authorRepository;

    @Spy
    @InjectMocks
    private AuthorController authorController;

    private Author author;

    @BeforeEach
    void setUp() {
        initMocks(this);

        author = new Author();
        author.setName("Jane");
        author.setSurname("Doe");
        author.setBirthDate(LocalDate.parse("22/11/1992", DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        author.setId(0);
    }

    @Test
    void getAuthor() {
        when(authorRepository.findById(0)).thenReturn(author);
        assertThat(authorController.getAuthor(0)).hasFieldOrPropertyWithValue("body", author);
        verify(authorRepository).findById(0);
    }

    @Test
    void postAuthor() {
        when(authorRepository.save(author)).thenReturn(author);
        assertThat(authorController.postAuthor(author.getName(), author.getSurname(), author.getBirthDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))))
                .hasFieldOrPropertyWithValue("body", "").hasFieldOrPropertyWithValue("statusCode", HttpStatus.OK);
        verify(authorRepository).save(author);
    }

    @Test
    void putAuthor() {
        Author returnAuthor = author;
        returnAuthor.setName("Janice");
        returnAuthor.setSurname("Dane");

        when(authorRepository.findById(0)).thenReturn(author);
        when(authorRepository.save(returnAuthor)).thenReturn(returnAuthor);

        assertThat(authorController.putAuthor(0, "Janice",
                "Dane",""))
                .hasFieldOrPropertyWithValue("body", "").hasFieldOrPropertyWithValue("statusCode", HttpStatus.OK);

        verify(authorRepository).findById(0);
        verify(authorRepository).save(returnAuthor);
    }

    @Test
    void deleteAuthor() {
        when(authorRepository.findById(0)).thenReturn(author);
        doNothing().when(authorRepository).delete(author);

        assertThat(authorController.deleteAuthor(0))
                .hasFieldOrPropertyWithValue("body", "").hasFieldOrPropertyWithValue("statusCode", HttpStatus.OK);

        verify(authorRepository).findById(0);
        verify(authorRepository).delete(author);
    }
}