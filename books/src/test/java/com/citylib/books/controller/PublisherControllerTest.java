package com.citylib.books.controller;

import com.citylib.books.dao.PublisherRepository;
import com.citylib.books.model.Publisher;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.http.HttpStatus;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

class PublisherControllerTest {
    @Mock
    private PublisherRepository publisherRepository;

    @Spy
    @InjectMocks
    private PublisherController publisherController;

    private Publisher publisher;

    @BeforeEach
    void setUp() {
        initMocks(this);

        publisher = new Publisher();
        publisher.setId(0);
        publisher.setName("Books & Co.");
    }

    @Test
    void getPublisher() {
        when(publisherRepository.findById(0)).thenReturn(publisher);
        assertThat(publisherController.getPublisher(0)).hasFieldOrPropertyWithValue("body", publisher);
        verify(publisherRepository).findById(0);
    }

    @Test
    void postPublisher() {
        when(publisherRepository.save(publisher)).thenReturn(publisher);
        assertThat(publisherController.postPublisher(publisher.getName()))
                .hasFieldOrPropertyWithValue("body", "").hasFieldOrPropertyWithValue("statusCode", HttpStatus.OK);
        verify(publisherRepository).save(publisher);
    }

    @Test
    void putAuthor() {
        Publisher returnPublisher = publisher;
        returnPublisher.setName("Brooks&Co");

        when(publisherRepository.findById(0)).thenReturn(publisher);
        when(publisherRepository.save(publisher)).thenReturn(returnPublisher);

        assertThat(publisherController.putPublisher(0, "Brooks&Co"))
                .hasFieldOrPropertyWithValue("body", "").hasFieldOrPropertyWithValue("statusCode", HttpStatus.OK);

        verify(publisherRepository).findById(0);
        verify(publisherRepository).save(returnPublisher);
    }

    @Test
    void deleteAuthor() {
        when(publisherRepository.findById(0)).thenReturn(publisher);
        doNothing().when(publisherRepository).delete(publisher);

        assertThat(publisherController.deletePublisher(0))
                .hasFieldOrPropertyWithValue("body", "").hasFieldOrPropertyWithValue("statusCode", HttpStatus.OK);

        verify(publisherRepository).findById(0);
        verify(publisherRepository).delete(publisher);
    }
}