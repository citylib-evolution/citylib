package com.citylib.batchmailer.job;

import com.citylib.batchmailer.config.ServiceRoutes;
import com.citylib.batchmailer.util.UtilGets;
import com.citylib.batchmailer.dto.Book;
import com.citylib.batchmailer.dto.BookReservation;
import com.citylib.batchmailer.dto.Library;
import com.citylib.batchmailer.dto.User;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.mail.MailProperties;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

@Component
public class ReservationAlertJob implements Job {
    private final MailProperties mailProperties;
    private final ServiceRoutes serviceRoutes;
    private final RestTemplate restTemplate;
    private final UtilGets utilGets;

    private static final Logger logger = LoggerFactory.getLogger(ReservationAlertJob.class);
    private ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    public ReservationAlertJob(MailProperties mailProperties, ServiceRoutes serviceRoutes, RestTemplate restTemplate, UtilGets utilGets) {
        this.mailProperties = mailProperties;
        this.serviceRoutes = serviceRoutes;
        this.restTemplate = restTemplate;
        this.utilGets = utilGets;
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        try {
            reservationAlertMail();
        } catch (IOException e) {
            logger.error("Reservation ready batch for reservationAlert failed");
        }
    }

    public void reservationAlertMail() throws IOException {
        // Retrieves list of rented copies past due date from library microservice
        ResponseEntity<String> responseReservationAlertsToBeSent = restTemplate.getForEntity(serviceRoutes.getReservationAlertsToBeSent(), String.class);
        List<BookReservation> reservationList = objectMapper.readValue(responseReservationAlertsToBeSent.getBody(), new TypeReference<List<BookReservation>>(){});

        // For each rented copy builds the information to be sent in the alert email
        for(BookReservation eachReservation : reservationList) {
            logger.info("Treating reservation with id " + eachReservation.getId() + " for book with id " + eachReservation.getBookId());
            Book book = utilGets.getBook(eachReservation.getBookId());
            User user = utilGets.getUser(eachReservation.getUserId());
            Library library = utilGets.getLibrary(eachReservation.getLibraryId());

            String subject = "Votre reservation est disponible";

            StringBuilder body = new StringBuilder();
            body.append("Bonjour ").append(user.getUsername()).append("<br><br>")
                .append("Merci d'avoir reservé le livre <b>").append(book.getTitle()).append("</b>,")
                .append("votre livre sera disponible le deux prochains jour dans la biblioteque ").append(library.getName()).append(".<br><br>")
                .append("Si vous sohaite d'annuler votre reservation, vous pouvais le faire depuis votre espace utilisateur de notre site.<br><br>")
                .append("Cordialement,<br>L'équipe de Citylib");

            boolean emailSent = utilGets.sendMail(mailProperties.getUsername(),user.getEmail(), subject, body.toString());
            if(emailSent) {
                restTemplate.getForEntity(serviceRoutes.getReservationNotified(eachReservation.getId()), String.class);
            }
        }
        logger.info("reservation alert batch has been concluded.");
    }
}
