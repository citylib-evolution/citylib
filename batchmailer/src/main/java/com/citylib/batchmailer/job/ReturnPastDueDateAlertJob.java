package com.citylib.batchmailer.job;

import com.citylib.batchmailer.config.ServiceRoutes;
import com.citylib.batchmailer.util.UtilGets;
import com.citylib.batchmailer.dto.Book;
import com.citylib.batchmailer.dto.RentedCopy;
import com.citylib.batchmailer.dto.User;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.mail.MailProperties;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.List;

@Component
public class ReturnPastDueDateAlertJob implements Job {
    private final MailProperties mailProperties;
    private final ServiceRoutes serviceRoutes;
    private final RestTemplate restTemplate;
    private final UtilGets utilGets;

    private static final Logger logger = LoggerFactory.getLogger(ReturnPastDueDateAlertJob.class);
    private ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    public ReturnPastDueDateAlertJob(MailProperties mailProperties, ServiceRoutes serviceRoutes, RestTemplate restTemplate, UtilGets utilGets) {
        this.mailProperties = mailProperties;
        this.serviceRoutes = serviceRoutes;
        this.restTemplate = restTemplate;
        this.utilGets = utilGets;
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        try {
            sendReturnPastDueDateAlertMail();
        } catch (IOException e) {
            logger.error("Return alert batch for pastDueDate failed");
        }
    }

    public void sendReturnPastDueDateAlertMail() throws IOException {
        // Retrieves list of rented copies past due date from library microservice
        ResponseEntity<String> responseDueTomorrow = restTemplate.getForEntity(serviceRoutes.getLibraryPastDueDate(), String.class);
        List<RentedCopy> pastDueDateList = objectMapper.readValue(responseDueTomorrow.getBody(), new TypeReference<List<RentedCopy>>(){});

        // For each rented copy builds the information to be sent in the alert email
        for(RentedCopy rentedCopy : pastDueDateList) {
            Book book = utilGets.getBook(rentedCopy.getBookCopy().getBookId());
            User user = utilGets.getUser(rentedCopy.getUserId());

            String subject = "Votre emprunt de " + book.getTitle() + " a depassé la date de retour.";
            StringBuilder body = new StringBuilder();
            body.append("Bonjour ").append(user.getUsername()).append("<br><br>")
                .append("La période d'emprunt pour le livre <b>").append(book.getTitle()).append("</b> est terminée.<br>")
                .append("Nous vous remercions de retourner le livre au plus vite.<br><br>")
                .append("Les retards de retours peuvent entraîner une pénalité.<br><br>")
                .append("Cordialement,<br>L'équipe de Citylib");

            boolean mailResult = utilGets.sendMail(mailProperties.getUsername(),user.getEmail(), subject, body.toString());
            if(mailResult) {
                logger.info("Email successfully sent to {}", user.getEmail());
            }

        }
        logger.info("pastDueDate alert batch has been concluded.");
    }
}
