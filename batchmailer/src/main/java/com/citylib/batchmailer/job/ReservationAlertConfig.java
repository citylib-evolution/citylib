package com.citylib.batchmailer.job;

import org.quartz.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:application.properties")
public class ReservationAlertConfig {
    @Value("${scheduler.timer.reservationAlertTime}")
    private String cronTime;

    // Builds job for batch sending emails with past due date alert
    @Bean
    public JobDetail reservationAlertDetails() {
        return JobBuilder.newJob(ReservationAlertJob.class).withIdentity("reservationAlertJob", "CityLibGroup").storeDurably().build();
    }

    // Trigger for the job, takes schedule time value from configuration file
    @Bean
    public Trigger reservationAlertTrigger(JobDetail reservationAlertDetails) {
        return TriggerBuilder.newTrigger()
                .withIdentity("fireForReservationAlerts", "CityLibGroup")
                .withSchedule(CronScheduleBuilder.cronSchedule(cronTime))
                .forJob(reservationAlertDetails)
                .build();
    }
}
