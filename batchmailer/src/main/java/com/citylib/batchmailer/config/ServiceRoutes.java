package com.citylib.batchmailer.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Getter
@PropertySource("classpath:application.properties")
@Component
public class ServiceRoutes {
    @Value("${service.book}")
    private String books;

    @Value("${service.library}")
    private String library;

    @Value("${service.webapp}")
    private String webapp;

    public String getLibraryDueTomorrow() {
        return library + "batchOps/dueTomorrow";
    }

    public String getLibraryPastDueDate() {
        return library + "batchOps/pastDueDate";
    }

    public String getReservationAlertsToBeSent() {
        return library + "batchOps/reservationAlertsToBeSent";
    }

    public String getReservationNotified(long reservationId) {
        return library + "batchOps/reservationNotified?reservationId=" + reservationId;
    }

    public String getBookById(long id) {
        return books + id;
    }

    public String getUser(long id) {
        return webapp + id;
    }

    public String getLibraryById(long id) { return library + id; }
}
