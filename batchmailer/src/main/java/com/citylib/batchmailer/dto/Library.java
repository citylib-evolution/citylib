package com.citylib.batchmailer.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.springframework.http.ResponseEntity;

import java.io.IOException;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Library {
    private long id;
    private String name;
}
