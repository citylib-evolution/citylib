package com.citylib.batchmailer.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class BookReservation {
    private long id;

    private long userId;

    private long bookId;

    private long libraryId;

    private String reservedTime;
}
