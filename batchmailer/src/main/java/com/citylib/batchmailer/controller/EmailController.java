package com.citylib.batchmailer.controller;

import com.citylib.batchmailer.config.ServiceRoutes;
import com.citylib.batchmailer.util.UtilGets;
import com.citylib.batchmailer.dto.Book;
import com.citylib.batchmailer.dto.Library;
import com.citylib.batchmailer.dto.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.mail.MailProperties;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

@RestController
@RequestMapping("/api/emailbatch")
public class EmailController {
    private final  MailProperties mailProperties;
    private final UtilGets utilGets;

    private static final Logger logger = LoggerFactory.getLogger(EmailController.class);

    @Autowired
    public EmailController(MailProperties mailProperties, UtilGets utilGets) {
        this.mailProperties = mailProperties;
        this.utilGets = utilGets;
    }

    // End point meant to send confirmation email for user upon registration
    @GetMapping("/confirmAccount")
    public ResponseEntity<String> sendConfirmationEmail(
            @RequestParam("authString") String authString,
            @RequestParam("recipient") String recipient
            ) {
        String subject = "Email de confirmation de compte pour CityLib";
        String emailBody = "Merci d'avoir créé un compte avec nous.<br>" +
                "Activez votre compte <a href='http://localhost:8080/accountConfirmation/" + authString + "'>ici</a>.<br>" +
                "<br> Nos meilleures salutations,<br>L'équipe de Citylib";
        if(utilGets.sendMail(mailProperties.getUsername(), recipient, subject, emailBody)) {
            return new ResponseEntity<>("true", HttpStatus.OK);
        } else {
            logger.error("Failed to send email to {}", mailProperties.getUsername());
            return new ResponseEntity<>("false", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/reservationAlert")
    public ResponseEntity<String> sendReservationAlertEmail(
            @RequestParam("userId") long userId,
            @RequestParam("bookId") long bookId,
            @RequestParam("libraryId") long libraryId
    ) throws IOException {
        Book book = utilGets.getBook(bookId);
        User user = utilGets.getUser(userId);
        Library library = utilGets.getLibrary(libraryId);

        String subject = "Votre reservation est disponible de mantenant";
        String emailBody = "Bonjour " + user.getUsername() + ",<br>" +
                "Merci d'avoir reservé un livre avec nous.<br>" +
                "Votre examplaire de " + book.getTitle() + " est actuelement disponible chez la bibliotheque " + library.getName() + ".<br>" +
                "Vous avez un periode de 48 heurs pour venir chercher votre examplaire ou votre reservation sera<br>" +
                "automatiquement suprimé.<br>" +
                "<br> Nos meilleures salutations,<br>L'équipe de Citylib";

        if(utilGets.sendMail(mailProperties.getUsername(), user.getEmail(), subject, emailBody)) {
            return new ResponseEntity<>("true", HttpStatus.OK);
        } else {
            logger.error("Failed to send email to {}", mailProperties.getUsername());
            return new ResponseEntity<>("false", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/reservationSupressed")
    public ResponseEntity<String> reservationSupressedAlertEmail(
            @RequestParam("userId") long userId,
            @RequestParam("bookId") long bookId
    ) throws IOException {
        Book book = utilGets.getBook(bookId);
        User user = utilGets.getUser(userId);

        String subject = "Votre reservation est desormais indisponible";
        String emailBody = "Bonjour " + user.getUsername() + ",<br>" +
                "Merci d'avoir reservé le livre " + book.getTitle() + " avec nous,<br>" +
                "malheureusement vous n'avez recuperé votre reservation dans le temps prevú et la reservation a etait suprimé<br>" +
                "et ne sera plus disponible.<br>" +
                "La non recuperation systematique de vos reservations peuvent entraîner une pénalité.<br>" +
                "<br> Nos meilleures salutations,<br>L'équipe de Citylib";

        if(utilGets.sendMail(mailProperties.getUsername(), user.getEmail(), subject, emailBody)) {
            return new ResponseEntity<>("true", HttpStatus.OK);
        } else {
            logger.error("Failed to send email to {}", mailProperties.getUsername());
            return new ResponseEntity<>("false", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
