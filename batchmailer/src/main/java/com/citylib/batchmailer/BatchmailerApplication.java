package com.citylib.batchmailer;

import com.citylib.batchmailer.job.ReservationAlertJob;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class BatchmailerApplication {

	public static void main(String[] args) {
		SpringApplication.run(BatchmailerApplication.class, args);
	}

}
