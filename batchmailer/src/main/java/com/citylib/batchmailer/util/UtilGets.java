package com.citylib.batchmailer.util;

import com.citylib.batchmailer.config.ServiceRoutes;
import com.citylib.batchmailer.dto.Book;
import com.citylib.batchmailer.dto.Library;
import com.citylib.batchmailer.dto.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

@Component
public class UtilGets {
    private final ServiceRoutes serviceRoutes;
    private final JavaMailSender mailSender;
    private final RestTemplate restTemplate;

    private static final Logger logger = LoggerFactory.getLogger(UtilGets.class);

    @Autowired
    public UtilGets(ServiceRoutes serviceRoutes, JavaMailSender mailSender, RestTemplate restTemplate) {
        this.serviceRoutes = serviceRoutes;
        this.mailSender = mailSender;
        this.restTemplate = restTemplate;
    }

    public Book getBook(long bookId) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();

        ResponseEntity<String> responseBook = restTemplate.getForEntity(serviceRoutes.getBookById(bookId), String.class);
        return objectMapper.readValue(responseBook.getBody(), Book.class);
    }

    public User getUser(long userId) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        ResponseEntity<String> responseUser = restTemplate.getForEntity(serviceRoutes.getUser(userId), String.class);
        return objectMapper.readValue(responseUser.getBody(), User.class);
    }

    public Library getLibrary(long libraryId) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        ResponseEntity<String> responseLibrary = restTemplate.getForEntity(serviceRoutes.getLibraryById(libraryId), String.class);
        return objectMapper.readValue(responseLibrary.getBody(), Library.class);
    }

    public Boolean sendMail(String fromEmail, String toEmail, String subject, String body) {
        try {
            logger.info("Sending email to {}", toEmail);
            MimeMessage message = mailSender.createMimeMessage();

            MimeMessageHelper messageHelper = new MimeMessageHelper(message, StandardCharsets.UTF_8.toString());
            messageHelper.setSubject(subject);
            messageHelper.setText(body, true);
            messageHelper.setFrom(fromEmail);
            messageHelper.setTo(toEmail);

            mailSender.send(message);
            return true;
        } catch (MessagingException ex) {
            logger.error("Failed to send email to {}", toEmail);
            return false;
        }
    }
}
