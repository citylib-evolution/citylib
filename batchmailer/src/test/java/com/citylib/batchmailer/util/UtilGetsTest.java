package com.citylib.batchmailer.util;

import com.citylib.batchmailer.config.ServiceRoutes;
import com.citylib.batchmailer.dto.Book;
import com.citylib.batchmailer.dto.Library;
import com.citylib.batchmailer.dto.User;
import com.citylib.batchmailer.job.ReturnPastDueDateAlertJob;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.boot.autoconfigure.mail.MailProperties;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.client.RestTemplate;

import javax.mail.internet.MimeMessage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

class UtilGetsTest {
    @Mock
    private ServiceRoutes serviceRoutes;
    @Mock
    private JavaMailSender mailSender;
    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private UtilGets utilGets;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @Test
    void getBook() throws IOException {
        // Builds response from json fixture
        InputStream inputStream = new ClassPathResource("fixtures/getBook.json").getInputStream();
        InputStreamReader isReader = new InputStreamReader(inputStream);
        BufferedReader reader = new BufferedReader(isReader);
        StringBuilder jsonResponseSB = new StringBuilder();
        String str;
        while((str = reader.readLine())!= null){
            jsonResponseSB.append(str).append("\n");
        }

        doReturn("bookUrl").when(serviceRoutes).getBookById(50);
        when(restTemplate.getForEntity(anyString(), ArgumentMatchers.any(Class.class))).thenReturn(new ResponseEntity<>(jsonResponseSB.toString(), HttpStatus.OK));

        Book book = utilGets.getBook(50);

        assertThat(book).hasSameClassAs(new Book()).isNotNull().hasFieldOrPropertyWithValue("id", Long.parseLong("1"));

        verify(serviceRoutes).getBookById(50);
        verify(restTemplate).getForEntity(serviceRoutes.getBookById(50), String.class);
    }

    @Test
    void getUser() throws IOException {
        // Builds response from json fixture
        InputStream inputStream = new ClassPathResource("fixtures/getUser.json").getInputStream();
        InputStreamReader isReader = new InputStreamReader(inputStream);
        BufferedReader reader = new BufferedReader(isReader);
        StringBuilder jsonResponseSB = new StringBuilder();
        String str;
        while((str = reader.readLine())!= null){
            jsonResponseSB.append(str).append("\n");
        }

        doReturn("userUrl").when(serviceRoutes).getUser(6);
        when(restTemplate.getForEntity(anyString(), ArgumentMatchers.any(Class.class))).thenReturn(new ResponseEntity<>(jsonResponseSB.toString(), HttpStatus.OK));

        User user = utilGets.getUser(6);

        assertThat(user).hasSameClassAs(new User()).isNotNull().hasFieldOrPropertyWithValue("id", Long.parseLong("1"));

        verify(serviceRoutes).getUser(6);
        verify(restTemplate).getForEntity(serviceRoutes.getUser(6), String.class);
    }

    @Test
    void getLibrary() throws IOException {
        // Builds response from json fixture
        InputStream inputStream = new ClassPathResource("fixtures/getLibrary.json").getInputStream();
        InputStreamReader isReader = new InputStreamReader(inputStream);
        BufferedReader reader = new BufferedReader(isReader);
        StringBuilder jsonResponseSB = new StringBuilder();
        String str;
        while((str = reader.readLine())!= null){
            jsonResponseSB.append(str).append("\n");
        }

        doReturn("libraryUrl").when(serviceRoutes).getLibraryById(1);
        when(restTemplate.getForEntity(anyString(), ArgumentMatchers.any(Class.class))).thenReturn(new ResponseEntity<>(jsonResponseSB.toString(), HttpStatus.OK));

        Library library = utilGets.getLibrary(1);

        assertThat(library).hasSameClassAs(new Library()).isNotNull().hasFieldOrPropertyWithValue("id", Long.parseLong("1"));

        verify(serviceRoutes).getLibraryById(1);
        verify(restTemplate).getForEntity(serviceRoutes.getLibraryById(1), String.class);
    }

    @Test
    void sendMail() {

        MimeMessage mimeMessage = mock(MimeMessage.class);
        doNothing().when(mailSender).send(any(MimeMessage.class));
        when(mailSender.createMimeMessage()).thenReturn(mimeMessage);

        boolean messageSent = utilGets.sendMail("sender@mail.com", "receiver@mail.com", "Test mail", "empty message");

        assertThat(messageSent).isTrue();

        verify(mailSender).send(any(MimeMessage.class));
    }
}