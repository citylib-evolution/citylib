package com.citylib.batchmailer.controller;

import com.citylib.batchmailer.config.ServiceRoutes;
import com.citylib.batchmailer.dto.Book;
import com.citylib.batchmailer.dto.Library;
import com.citylib.batchmailer.dto.User;
import com.citylib.batchmailer.job.ReservationAlertJob;
import com.citylib.batchmailer.util.UtilGets;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.boot.autoconfigure.mail.MailProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class EmailControllerTest {
    @Mock
    private MailProperties mailProperties;
    @Mock
    private UtilGets utilGets;

    private User user;
    private Book book;
    private Library library;

    @Spy
    @InjectMocks
    private EmailController emailController;

    @BeforeEach
    void setUp() {
        initMocks(this);

        user = new User();
        user.setId(1);
        user.setEmail("citylib_user5@yopmail.com");
        user.setUsername("user5");

        book = new Book();
        book.setId(1);
        book.setTitle("Heatstroke");

        library = new Library();
        library.setId(1);
        library.setName("Random Lib");
    }

    @Test
    void sendConfirmationEmail() throws IOException {
        String recipient = "randommail@mail.com";
        String authString = "authString";
        when(mailProperties.getUsername()).thenReturn("citylib@mail.com");

        when(utilGets.sendMail("citylib@mail.com", recipient, "Email de confirmation de compte pour CityLib",
                "Merci d'avoir créé un compte avec nous.<br>" +
                "Activez votre compte <a href='http://localhost:8080/accountConfirmation/" + authString + "'>ici</a>.<br>" +
                "<br> Nos meilleures salutations,<br>L'équipe de Citylib")).thenReturn(true);

        assertThat(emailController.sendConfirmationEmail("authString", "randommail@mail.com"))
                .hasFieldOrPropertyWithValue("body", "true")
                .hasFieldOrPropertyWithValue("statusCode", HttpStatus.OK);
    }

    @Test
    void sendReservationAlertEmail() throws IOException {
        doReturn(book).when(utilGets).getBook(1);
        doReturn(user).when(utilGets).getUser(1);
        doReturn(library).when(utilGets).getLibrary(1);
        when(mailProperties.getUsername()).thenReturn("citylib@mail.com");

        when(utilGets.sendMail("citylib@mail.com", user.getEmail(), "Votre reservation est disponible de mantenant",
                "Bonjour " + user.getUsername() + ",<br>" +
                        "Merci d'avoir reservé un livre avec nous.<br>" +
                        "Votre examplaire de " + book.getTitle() + " est actuelement disponible chez la bibliotheque " + library.getName() + ".<br>" +
                        "Vous avez un periode de 48 heurs pour venir chercher votre examplaire ou votre reservation sera<br>" +
                        "automatiquement suprimé.<br>" +
                        "<br> Nos meilleures salutations,<br>L'équipe de Citylib")).thenReturn(true);

        assertThat(emailController.sendReservationAlertEmail(1,1,1))
                .hasFieldOrPropertyWithValue("body", "true")
                .hasFieldOrPropertyWithValue("statusCode", HttpStatus.OK);
    }

    @Test
    void reservationSupressedAlertEmail() throws IOException {
        doReturn(book).when(utilGets).getBook(1);
        doReturn(user).when(utilGets).getUser(1);
        doReturn(library).when(utilGets).getLibrary(1);
        when(mailProperties.getUsername()).thenReturn("citylib@mail.com");

        when(utilGets.sendMail("citylib@mail.com", user.getEmail(), "Votre reservation est desormais indisponible",
                "Bonjour " + user.getUsername() + ",<br>" +
                "Merci d'avoir reservé le livre " + book.getTitle() + " avec nous,<br>" +
                "malheureusement vous n'avez recuperé votre reservation dans le temps prevú et la reservation a etait suprimé<br>" +
                "et ne sera plus disponible.<br>" +
                "La non recuperation systematique de vos reservations peuvent entraîner une pénalité.<br>" +
                "<br> Nos meilleures salutations,<br>L'équipe de Citylib")).thenReturn(true);

        assertThat(emailController.reservationSupressedAlertEmail(1,1))
                .hasFieldOrPropertyWithValue("body", "true")
                .hasFieldOrPropertyWithValue("statusCode", HttpStatus.OK);
    }
}