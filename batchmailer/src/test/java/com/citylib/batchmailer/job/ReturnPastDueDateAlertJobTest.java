package com.citylib.batchmailer.job;

import com.citylib.batchmailer.config.ServiceRoutes;
import com.citylib.batchmailer.util.UtilGets;
import com.citylib.batchmailer.dto.Book;
import com.citylib.batchmailer.dto.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.boot.autoconfigure.mail.MailProperties;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import static org.assertj.core.api.Assertions.assertThatCode;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

class ReturnPastDueDateAlertJobTest {
    @Mock
    private MailProperties mailProperties;
    @Mock
    private ServiceRoutes serviceRoutes;
    @Mock
    private RestTemplate restTemplate;
    @Mock
    private UtilGets utilGets;

    private User user;
    private Book book;

    @Spy
    @InjectMocks
    private ReturnPastDueDateAlertJob returnPastDueDateAlertJob;

    @BeforeEach
    void setUp() {
        initMocks(this);

        user = new User();
        user.setId(1);
        user.setEmail("citylib_user5@yopmail.com");
        user.setUsername("user5");

        book = new Book();
        book.setId(1);
        book.setTitle("Heatstroke");
    }

    @Test
    void sendReturnPastDueDateAlertMail() throws IOException {
        // Builds response from json fixture
        InputStream inputStream = new ClassPathResource("fixtures/returnPastDueDate.json").getInputStream();
        InputStreamReader isReader = new InputStreamReader(inputStream);
        BufferedReader reader = new BufferedReader(isReader);
        StringBuilder jsonResponseSB = new StringBuilder();
        String str;
        while((str = reader.readLine())!= null){
            jsonResponseSB.append(str).append("\n");
        }

        when(serviceRoutes.getLibraryPastDueDate()).thenReturn("libraryUrl");
        when(restTemplate.getForEntity(anyString(), ArgumentMatchers.any(Class.class))).thenReturn(new ResponseEntity<>(jsonResponseSB.toString(), HttpStatus.OK));
        doReturn(book, book).doReturn(book).when(utilGets).getBook(1);
        doReturn(user, user).doReturn(user).when(utilGets).getUser(1);
        when(mailProperties.getUsername()).thenReturn("citylib@mail.com");

        doReturn(true, true).when(utilGets).sendMail(anyString(), anyString(), anyString(), anyString());

        assertThatCode(returnPastDueDateAlertJob::sendReturnPastDueDateAlertMail).doesNotThrowAnyException();

        verify(restTemplate).getForEntity(serviceRoutes.getLibraryPastDueDate(), String.class);
        verify(utilGets, times(2)).sendMail(
                "citylib@mail.com",
                user.getEmail(),
                "Votre emprunt de Heatstroke a depassé la date de retour.",
                "Bonjour user5<br><br>La période d'emprunt pour le livre <b>Heatstroke</b> est terminée.<br>Nous vous remercions de retourner le livre au plus vite.<br><br>Les retards de retours peuvent entraîner une pénalité.<br><br>Cordialement,<br>L'équipe de Citylib"
        );
    }
}