package com.citylib.batchmailer.job;

import com.citylib.batchmailer.config.ServiceRoutes;
import com.citylib.batchmailer.dto.Book;
import com.citylib.batchmailer.dto.Library;
import com.citylib.batchmailer.dto.User;
import com.citylib.batchmailer.util.UtilGets;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.boot.autoconfigure.mail.MailProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import static org.assertj.core.api.Assertions.assertThatCode;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

class ReservationAlertJobTest {
    @Mock
    private MailProperties mailProperties;
    @Mock
    private ServiceRoutes serviceRoutes;
    @Mock
    private RestTemplate restTemplate;
    @Mock
    private UtilGets utilGets;

    private User user;
    private Book book;
    private Library library;

    @Spy
    @InjectMocks
    private ReservationAlertJob reservationAlertJob;

    @BeforeEach
    void setUp() {
        initMocks(this);

        user = new User();
        user.setId(1);
        user.setEmail("citylib_user5@yopmail.com");
        user.setUsername("user5");

        book = new Book();
        book.setId(1);
        book.setTitle("Heatstroke");

        library = new Library();
        library.setId(1);
        library.setName("Random Lib");
    }

    @Test
    void reservationAlertMail() throws IOException {
        // Builds response from json fixture
        InputStream inputStream = new ClassPathResource("fixtures/reservationAlerts.json").getInputStream();
        InputStreamReader isReader = new InputStreamReader(inputStream);
        BufferedReader reader = new BufferedReader(isReader);
        StringBuilder jsonResponseSB = new StringBuilder();
        String str;
        while((str = reader.readLine())!= null){
            jsonResponseSB.append(str).append("\n");
        }

        when(serviceRoutes.getReservationAlertsToBeSent()).thenReturn("alertsToBeSent");
        when(serviceRoutes.getReservationNotified(1001)).thenReturn("notified1001");
        when(serviceRoutes.getReservationNotified(1002)).thenReturn("notified1002");
        when(restTemplate.getForEntity(serviceRoutes.getReservationAlertsToBeSent(), String.class)).thenReturn(new ResponseEntity<>(jsonResponseSB.toString(), HttpStatus.OK));

        when(restTemplate.getForEntity(serviceRoutes.getReservationNotified(1001), String.class)).thenReturn(null);
        when(restTemplate.getForEntity(serviceRoutes.getReservationNotified(1002), String.class)).thenReturn(null);
        doReturn(book, book).when(utilGets).getBook(1);
        doReturn(user, user).when(utilGets).getUser(1);
        doReturn(library, library).when(utilGets).getLibrary(1);
        when(mailProperties.getUsername()).thenReturn("citylib@mail.com");

        doReturn(true, true).when(utilGets).sendMail(anyString(), anyString(), anyString(), anyString());

        assertThatCode(reservationAlertJob::reservationAlertMail).doesNotThrowAnyException();

        verify(restTemplate).getForEntity("alertsToBeSent", String.class);
        verify(utilGets, times(2)).sendMail(
                "citylib@mail.com",
                user.getEmail(),
                "Votre reservation est disponible",
                "Bonjour user5<br><br>Merci d'avoir reservé le livre <b>Heatstroke</b>,votre livre sera disponible le deux prochains jour dans la biblioteque Random Lib.<br><br>Si vous sohaite d'annuler votre reservation, vous pouvais le faire depuis votre espace utilisateur de notre site.<br><br>Cordialement,<br>L'équipe de Citylib"
        );
    }
}