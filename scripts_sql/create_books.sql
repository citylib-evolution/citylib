create sequence hibernate_sequence start 1 increment 1
create table author (id int8 not null, birth_date date, name varchar(255), surname varchar(255), primary key (id))
create table book (id int8 not null, available_qty int4 not null, description text, release_date date, title varchar(255), author_id int8, publisher_id int8, primary key (id))
create table publisher (id int8 not null, name varchar(255), primary key (id))
alter table book add constraint FKklnrv3weler2ftkweewlky958 foreign key (author_id) references author
alter table book add constraint FKgtvt7p649s4x80y6f4842pnfq foreign key (publisher_id) references publisher

-- Showcase data bellow
-- Authors
insert into author (id, name, surname, birth_date) values (1, 'Kimmie', 'Innerstone', to_date('26/1/1937', 'DD/MM/YYYY'));
insert into author (id, name, surname, birth_date) values (2, 'Rosene', 'Stollery', to_date('4/4/1970', 'DD/MM/YYYY'));
insert into author (id, name, surname, birth_date) values (3, 'Shirlene', 'Yakob', to_date('27/2/1936', 'DD/MM/YYYY'));
insert into author (id, name, surname, birth_date) values (4, 'Renato', 'Blazejewski', to_date('5/4/1967', 'DD/MM/YYYY'));
insert into author (id, name, surname, birth_date) values (5, 'Flossi', 'Penquet', to_date('10/3/1930', 'DD/MM/YYYY'));
insert into author (id, name, surname, birth_date) values (6, 'Victoir', 'O''Scully', to_date('5/2/1933', 'DD/MM/YYYY'));
insert into author (id, name, surname, birth_date) values (7, 'Lucio', 'Lomansey', to_date('3/4/1998', 'DD/MM/YYYY'));
insert into author (id, name, surname, birth_date) values (8, 'Annora', 'Georgelin', to_date('5/9/1988', 'DD/MM/YYYY'));
insert into author (id, name, surname, birth_date) values (9, 'Seana', 'Gemeau', to_date('15/12/1925', 'DD/MM/YYYY'));
insert into author (id, name, surname, birth_date) values (10, 'Keriann', 'Oxx', to_date('26/12/1923', 'DD/MM/YYYY'));

-- Publishers
insert into publisher (id, name) values (1, 'Brainverse');
insert into publisher (id, name) values (2, 'Aimbu');
insert into publisher (id, name) values (3, 'Centimia');
insert into publisher (id, name) values (4, 'Livepath');
insert into publisher (id, name) values (5, 'Devbug');
insert into publisher (id, name) values (6, 'Eabox');
insert into publisher (id, name) values (7, 'Ailane');
insert into publisher (id, name) values (8, 'Rooxo');
insert into publisher (id, name) values (9, 'Reallinks');
insert into publisher (id, name) values (10, 'Vinder');

-- Books
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (1, 'Wanderers', 'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.', 10, to_date('14/7/2012', 'DD/MM/YYYY'), 6, 6);
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (2, 'His Private Secretary', 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', 8, to_date('4/3/1943', 'DD/MM/YYYY'), 9, 5);
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (3, 'Dinosaur', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.', 3, to_date('16/6/1954', 'DD/MM/YYYY'), 5, 8);
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (4, 'Trouble in Paradise', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.', 5, to_date('2/6/1922', 'DD/MM/YYYY'), 9, 2);
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (5, 'Boxing Helena', 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.', 2, to_date('22/7/1943', 'DD/MM/YYYY'), 10, 5);
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (6, 'Timeline', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 5, to_date('18/6/1979', 'DD/MM/YYYY'), 1, 6);
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (7, 'Spirited Away (Sen to Chihiro no kamikakushi)', 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.', 5, to_date('20/7/2000', 'DD/MM/YYYY'), 4, 10);
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (8, '3-Iron (Bin-jip)', 'Fusce consequat. Nulla nisl. Nunc nisl.', 1, to_date('28/8/2017', 'DD/MM/YYYY'), 5, 3);
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (9, 'Maiden''s Cheek (To xylo vgike apo ton Paradeiso)', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.', 4, to_date('18/11/2005', 'DD/MM/YYYY'), 2, 7);
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (10, 'Red Scorpion', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.', 2, to_date('17/5/1997', 'DD/MM/YYYY'), 8, 7);
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (11, 'Let It Snow', 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.', 6, to_date('19/4/2004', 'DD/MM/YYYY'), 3, 7);
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (12, 'Great Dictator, The', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.', 10, to_date('29/10/1956', 'DD/MM/YYYY'), 6, 7);
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (13, 'Mr. Troop Mom', 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.', 1, to_date('3/3/1952', 'DD/MM/YYYY'), 10, 10);
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (14, 'Year of the Dragon', 'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.', 6, to_date('3/7/2012', 'DD/MM/YYYY'), 3, 7);
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (15, 'Forest (Rengeteg)', 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', 4, to_date('29/6/1969', 'DD/MM/YYYY'), 1, 6);
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (16, 'We''re Not Dressing', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', 1, to_date('6/6/1979', 'DD/MM/YYYY'), 4, 8);
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (17, 'Odds, The', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.', 9, to_date('25/6/1987', 'DD/MM/YYYY'), 3, 2);
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (18, 'Born to Be Bad', 'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', 10, to_date('24/7/2006', 'DD/MM/YYYY'), 2, 9);
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (19, 'Plastic', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', 10, to_date('8/9/1991', 'DD/MM/YYYY'), 1, 7);
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (20, 'Essex Boys', 'Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.', 3, to_date('24/12/2018', 'DD/MM/YYYY'), 9, 8);
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (21, 'La Soufrière - Warten auf eine unausweichliche Katastrophe', 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', 2, to_date('1/11/1996', 'DD/MM/YYYY'), 8, 5);
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (22, 'Kon-Tiki', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.', 6, to_date('29/5/1982', 'DD/MM/YYYY'), 1, 7);
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (23, 'Waiting for Happiness (Heremakono)', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 3, to_date('14/9/1964', 'DD/MM/YYYY'), 3, 8);
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (24, 'Joe Gould''s Secret', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', 6, to_date('4/11/1984', 'DD/MM/YYYY'), 5, 7);
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (25, 'Juan of the Dead (Juan de los Muertos)', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.', 8, to_date('9/6/1979', 'DD/MM/YYYY'), 6, 10);
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (26, 'Gathering of Old Men, A (Murder on the Bayou)', 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.

Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 5, to_date('11/4/2008', 'DD/MM/YYYY'), 7, 7);
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (27, 'Cocktail', 'Fusce consequat. Nulla nisl. Nunc nisl.', 9, to_date('2/5/1965', 'DD/MM/YYYY'), 9, 5);
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (28, 'Girl 6', 'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.

Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.', 1, to_date('28/5/2010', 'DD/MM/YYYY'), 8, 8);
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (29, 'YellowBrickRoad', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.', 3, to_date('7/12/1949', 'DD/MM/YYYY'), 3, 1);
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (30, 'Crisis', 'Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.', 6, to_date('22/4/1922', 'DD/MM/YYYY'), 10, 10);
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (31, 'Molly', 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', 7, to_date('21/6/2004', 'DD/MM/YYYY'), 7, 1);
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (32, 'Taxi', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', 7, to_date('23/7/2018', 'DD/MM/YYYY'), 7, 4);
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (33, 'Emma', 'Fusce consequat. Nulla nisl. Nunc nisl.', 1, to_date('22/12/1933', 'DD/MM/YYYY'), 10, 7);
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (34, 'Sexual Life', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.', 4, to_date('14/11/1999', 'DD/MM/YYYY'), 3, 3);
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (35, 'In the Year of the Pig', 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', 4, to_date('11/12/1942', 'DD/MM/YYYY'), 3, 4);
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (36, 'Pollock', 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.', 2, to_date('24/3/2005', 'DD/MM/YYYY'), 6, 3);
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (37, 'Syrian Bride, The', 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.', 9, to_date('1/1/1992', 'DD/MM/YYYY'), 4, 6);
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (38, 'Cabin in the Sky', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', 6, to_date('23/4/1977', 'DD/MM/YYYY'), 10, 10);
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (39, 'Baby Doll', 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', 3, to_date('14/1/1977', 'DD/MM/YYYY'), 2, 5);
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (40, 'Meet Me in St. Louis', 'Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.', 9, to_date('3/6/2011', 'DD/MM/YYYY'), 10, 9);
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (41, 'Watermelon Man', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.', 9, to_date('10/7/1963', 'DD/MM/YYYY'), 9, 7);
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (42, 'Bethlehem', 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', 4, to_date('14/3/1984', 'DD/MM/YYYY'), 1, 3);
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (43, 'Orange County', 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.', 9, to_date('13/7/1952', 'DD/MM/YYYY'), 3, 9);
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (44, 'Mama', 'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.', 3, to_date('30/10/1949', 'DD/MM/YYYY'), 4, 2);
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (45, 'Sincerely Yours', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', 8, to_date('2/12/2004', 'DD/MM/YYYY'), 3, 5);
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (46, 'Hired Hand, The', 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', 10, to_date('24/9/1998', 'DD/MM/YYYY'), 9, 3);
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (47, 'Amador', 'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.', 4, to_date('21/3/2010', 'DD/MM/YYYY'), 10, 7);
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (48, 'Hitchhiker''s Guide to the Galaxy, The', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.', 4, to_date('27/5/1921', 'DD/MM/YYYY'), 2, 9);
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (49, 'Seconds Apart ', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.', 3, to_date('7/6/1943', 'DD/MM/YYYY'), 6, 3);
insert into book (id, title, description, available_qty, release_date, author_id, publisher_id) values (50, 'Heatstroke', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 7, to_date('19/6/1937', 'DD/MM/YYYY'), 4, 9);