create table book_reservation
(
    id              bigint  not null
        constraint book_reservation_pkey
            primary key,
    book_id         bigint  not null,
    library_id      bigint  not null,
    notified        boolean not null,
    notified_time   timestamp,
    placement       integer,
    ready_to_notify boolean not null,
    reserved_time   timestamp,
    user_id         bigint  not null
);